$.ready( function () {
    
    $.getHTML( 'hello/you.html' , function ( html ) {
        
        var overlay , welcome , resize ,
            events = new eventHandler();
        
        $( 'body' ).append( html );
        
        overlay = document.getElementById( 'overlay-welcome' );
        welcome = overlay.firstElementChild;
        
        resize = function () {
            var size = this.getBoundingClientRect();
            this.style.setProperty( 'left' , ( window.innerWidth - size.width ) / 2 + 'px' , '' );
            this.style.setProperty( 'top' , ( window.innerHeight / 2 ) - size.height / 1.2 + 'px' , '' );
        }.bind( welcome );
        
        events.listen( window , 'resize' , ( resize() , resize ) ), resize = null;
        
        setTimeout( function () {
            
            overlay.classList.add( 'open' );
            welcome.firstElementChild.classList.add( 'open' );
            
            setTimeout( function () {
                
                welcome.firstElementChild.classList.add( 'color' );
                document.getElementById( 'powered' ).classList.add( 'open' );
                document.getElementById( 'indicator' ).classList.add( 'animated' );
                
                setTimeout( function () {
                    
                    document.getElementById( 'indicator' ).classList.add( 'animated' );
                    
                    document.getElementById( 'container' ).style.setProperty( 'opacity' , '1' , '' );
                    
                    requireJS( [ 'js/engine.js' , 'js/visualizer.js' ] , function () {
                        
                        overlay.classList.remove( 'open' );
                        
                        setTimeout( function () {
                            
                            overlay.parentNode.removeChild( overlay );
                            
                            events.destroy() , events = null;
                            
                        } , 800 );
                    } );
                    
                } , 2000 );
                
            } , 1000 );
            
        } , 500 );
        
    } );
    
} );