/**
 * @author tla
 * @see {@link https://framagit.org/tla}
 *
 * css-rule
 * @see {@link https://framagit.org/tla/css-rule}
 *
 * @license MIT
 * @see {@link https://framagit.org/tla/css-rule/blob/master/LICENSE}
 *
 * Create CSS rules without Tree manipulation
 *
 * @method css.add
 * @argument {String} selector - css selector
 * @argument {String} property - css property
 * @return void
 *
 * @method css.remove
 * @argument {String} selector - css selector
 * @return void
 *
 * @method css.reset
 * @return void
 *
 * @example
 *    var css = new cssgen();
 *    css.add( 'p' , 'color:red' );
 *    css.add( 'a' , 'color:blue; text-decoration:underline' );
 *    css.remove( 'p' ); // remove only ^P$ selector
 *    css.destroy(); // remove all
 *
 * requirement :
 *    - chrome ( desktop & android ) 1.0+
 *    - opera ( desktop and mobile ) 1.0+
 *    - IE ( desktop & mobile ) 5+
 *    - safari ( osx & ios ) 1.0+
 *    - firefox 1.0+
 * */

window.cssgen = ( function () {
    
    var passiveSupport = false;
    
    // check if the browser support passive events
    ( function () {
        var tmp = document.createDocumentFragment();
        
        var empty = function () {} , setting = {
            get passive () {
                passiveSupport = true;
                return false;
            }
        };
        
        tmp.addEventListener( 'click' , empty , setting );
        
        empty = tmp = null;
    } )();
    
    var passiveArgument = function ( c ) {
        if ( passiveSupport ) {
            return {
                passive : true ,
                capture : c
            };
        }
        
        return c;
    };
    
    function exist ( _ ) {
        return _ !== undefined && _ !== null;
    }
    
    function isfunction ( _ ) {
        return exist( _ ) && typeof _ === 'function';
    }
    
    function sniffer ( instance , callback ) {
        var self = this;
        
        var random = '_' + Math.random().toString( 30 ).substring( 2 );
        
        this.element = instance.document.createElement( 'div' );
        
        this.window = instance.document.defaultView;
        
        this.rule = '#' + random;
        
        this.css = instance;
        
        this.element.setAttribute( 'style' ,
            'pointer-events: none;' +
            'visibility: hidden' +
            'z-index: -1;' +
            
            'position: relative;' +
            'display: block;' +
            
            'height: 10px;' +
            'width: 10px;'
        );
        
        this.element.id = random;
        
        instance.document.body.appendChild( this.element );
        
        this.resize = this.onresize( this.element , function () {
            callback();
            
            self.destroy();
        } );
        
        this.css.add( this.rule , 'width: 20px !important; height: 20px !important;' );
    }
    
    sniffer.prototype.destroy = function () {
        if ( this.element.parentNode ) {
            this.element.parentNode.removeChild( this.element );
        }
        
        if ( this.resize ) {
            this.resize();
            this.resize = null;
        }
        
        this.css.remove( this.rule );
    };
    
    sniffer.prototype.onresize = ( function () {
        var generic = 'top: 0; left: 0; right: 0; bottom: 0; position: absolute; overflow: scroll;' ,
            style = {
                scroller : generic ,
                expand : generic + 'width: 20px; height: 20px;' ,
                container : generic + 'visibility: hidden; pointer-events: none; z-index: -1;'
            };
        
        var pattern = ( function () {
            var container = document.createElement( 'DIV' ) ,
                doc = document.createDocumentFragment();
            
            container.style.cssText = style.container;
            
            container.innerHTML = '' +
                '<div style="' + style.scroller + '" >' +
                '   <div style="' + style.expand + '" ></div>' +
                '</div>';
            
            doc.appendChild( container );
            
            return doc;
        } )();
        
        function getsizeprop ( element , prop ) {
            return element[ 'client' + prop ] || element[ 'offset' + prop ] || 0;
        }
        
        function getsize ( element ) {
            return {
                width : getsizeprop( element , 'Width' ) ,
                height : getsizeprop( element , 'Height' )
            };
        }
        
        function reset ( scrollerExpand ) {
            scrollerExpand.scrollTop = scrollerExpand.scrollHeight;
        }
        
        return function ( element , callback ) {
            var container = pattern.cloneNode( true ) ,
                current = getsize( element ) ,
                window = this.window;
            
            function destroy () {
                window.removeEventListener( 'scroll' , eventHandler , true );
            }
            
            function change () {
                var tmp = getsize( element );
                
                if ( tmp.width != current.width || tmp.height != current.height ) {
                    destroy();
                    callback();
                }
            }
            
            function eventHandler ( e ) {
                if ( element.contains( e.target ) ) {
                    e.stopImmediatePropagation();
                    change();
                }
            }
            
            element.appendChild( container );
            
            reset( element.children[ 0 ].children[ 0 ] );
            
            window.addEventListener( 'scroll' , eventHandler , passiveArgument( true ) );
            
            return destroy;
        };
        
    } )();
    
    function cssgen ( doc ) {
        !doc && ( doc = document );
        
        this.id = 0;
        this.index = {};
        this.timeout = [];
        this.document = doc;
        
        this.style = doc.createElement( 'STYLE' );
        this.style.setAttribute( 'type' , 'text/css' );
        this.style.appendChild( doc.createTextNode( "" ) );
        
        this.head = doc.getElementsByTagName( 'HEAD' )[ 0 ];
        this.head.appendChild( this.style );
        
        this.api = this.style.sheet;
        this.rules = this.api.cssRules;
        this.insertRule = 'insertRule' in this.api ? 'insertRule' : 'addRule';
        this.deleteRule = 'deleteRule' in this.api ? 'deleteRule' : 'removeRule';
    }
    
    cssgen.prototype.setTimeout = function ( fn , time ) {
        var id = setTimeout( function () { this.timeout.splice( this.timeout.indexOf( id ) , 1 ) , fn(); } , time );
        this.timeout.push( id );
    };
    
    cssgen.prototype.clearTimeout = function () {
        this.timeout.forEach( function ( id ) { clearTimeout( id ); } );
    };
    
    cssgen.prototype.removeByRule = function ( rule ) {
        var rules = Array.apply( null , this.rules ) , i;
        
        if ( ( i = rules.indexOf( rule ) , !!~i ) ) {
            this.api[ this.deleteRule ]( i );
            
            return true;
        }
        
        return false;
    };
    
    /**
     * @method cssgen.prototype.add
     * @param {string} selector
     * @param {string} rules
     * @param {function} [call]
     * @return {void}
     * */
    cssgen.prototype.add = function ( selector , rules , call ) {
        var rule , self = this;
        
        this.api[ this.insertRule ]( selector + '{' + rules + '}' , this.rules.length );
        
        rule = this.rules[ this.rules.length - 1 ];
        
        if ( !this.index[ selector ] ) {
            this.index[ selector ] = {
                rules : [] ,
                callbacks : {}
            };
        }
        
        this.index[ selector ].rules.push( rule );
        
        if ( isfunction( call ) ) {
            var id = ++this.id;
            
            this.index[ selector ].callbacks[ id ] = new sniffer( this , function () {
                delete self.index[ selector ].callbacks[ id ];
                call();
            } );
        }
    };
    
    /**
     * @method cssgen.prototype.remove
     * @param {string} selector
     * @return {boolean}
     * */
    cssgen.prototype.remove = function ( selector ) {
        var result = true ,
            callbacks , array , i , l;
        
        if ( array = this.index[ selector ] ) {
            for ( i = 0, l = array.length ; i < l ; ) {
                if ( !this.removeByRule( array[ i++ ] ) ) {
                    result = false;
                }
            }
            
            callbacks = this.index[ selector ].callbacks;
            
            for ( i in callbacks ) {
                callbacks[ i ].destroy();
            }
            
            delete this.index[ selector ];
        }
        
        return result;
    };
    
    /**
     * @method cssgen.prototype.destroy
     * @return {void}
     * */
    cssgen.prototype.destroy = function () {
        this.clearTimeout();
        this.head.removeChild( this.style );
    };
    
    return cssgen;
    
} )();