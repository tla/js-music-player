/**
 * @author tla
 * @see {@link https://framagit.org/tla}
 *
 * awesome-bubble
 * @see {@link https://framagit.org/tla/awesome-bubble}
 *
 * @license MIT
 * @see {@link https://framagit.org/tla/awesome-bubble/blob/master/LICENSE}
 *
 * @use css-rule
 * @see {@link https://framagit.org/tla/css-rule}
 *
 * Material design bubble effect
 *
 * @method _awesomeBubble
 * @argument {HTMLElement} element
 * @argument {Object} [setting]
 * @return {constructor}
 *
 * @example
 *  var element = document.getElementById('toto');
 *  var bubble = _awesomeBubble( element ); // finish !
 *
 *  // setting
 *
 *  bubble = _awesomeBubble( element , {
 *      color: 'rgba(0,0,0,0.1)' // custom the bubble color, default "rgba(100, 255, 255, 0.3)"
 *      colorEnd: 'rgba(0,0,0,0.1)' // custom the bubble color transition, default null
 *      duration:   500 // custom the duration bubble effect in ms, default 700
 *      number: 3 // number of bubble per click, default 1
 *      interval: 150 // interval between bubbles ( if number > 1 ) in ms, default 100
 *      stayOnFocus: 150 // persistent bubble, default false
 *      document: document / iframe.contentDocument // document object
 *  } );
 *
 *  // can do a fucking rainbow ( and destroy your eyes )
 *  bubble = _awesomeBubble( element , {
 *      duration:   2000 ,
 *      color: [
 *          'rgb(50,0,0)' ,
 *          'rgb(150,0,0)' ,
 *          'rgb(250,0,0)' ,
 *          'rgb(0,50,0)' ,
 *          'rgb(0,150,0)' ,
 *          'rgb(0,250,0)' ,
 *          'rgb(0,0,50)' ,
 *          'rgb(0,0,150)' ,
 *          'rgb(0,0,250)'
 *      ]
 *  } );
 *
 *  // disable
 *  bubble.disable();
 *
 *  // enable
 *  bubble.enable();
 *
 *  // kill
 *  bubble.destroy();
 *
 *  // change settings dynamicaly (you can't change "document" setting)
 *  bubble.setParams( "color" , ... );
 * */

// TODO faire un readme

( function () {
    
    /*** @property {boolean} window.passiveEventAvailable */
    var passiveEventAvailable = false ,
        
        /**
         * @method window.passiveEventHandler
         * @param {boolean} [cap=false}
         * @return {boolean|Object}
         * */
        passiveEventHandler = function ( cap ) {
            return Boolean( cap );
        };
    
    // check passive event support
    ( function () {
        var empty = function () {};
        
        //noinspection JSCheckFunctionSignatures
        document.addEventListener( 'click' , empty , {
            get passive () {
                passiveEventAvailable = true;
                passiveEventHandler = function ( cap ) {
                    return {
                        capture : !!cap ,
                        passive : true
                    };
                };
                return false;
            }
        } );
        
        document.removeEventListener( 'click' , empty );
        
        empty = null;
    } )();
    
    // cross-browser trim
    var trim = ( function () {
            return !!''.trim ? function ( a ) {
                    return a.trim();
                } :
                function ( a ) {
                    return a.replace( /^[\s\u00A0]+|[\s\u00A0]+$/g , '' );
                };
        } )() ,
        // closest polyfill
        closest = ( function () {
            /**
             * @namespace HTMLElement
             * @property {function} HTMLElement.closest
             * */
            var tmp = document.createElement( 'div' ) ,
                match;
            
            if ( tmp.closest && tmp.closest.constructor === Function ) {
                return tmp = null,
                    function ( e , s ) {
                        return e.closest( s );
                    };
            }
            
            match = tmp.matches && 'matches' ||
                tmp.matchesSelector && 'matchesSelector' ||
                tmp.webkitMatchesSelector && 'webkitMatchesSelector' ||
                tmp.mozMatchesSelector && 'mozMatchesSelector' ||
                tmp.oMatchesSelector && 'oMatchesSelector' ||
                tmp.msMatchesSelector && 'msMatchesSelector';
            
            return tmp = null,
                function ( e , s ) {
                    if ( e[ match ]( s ) ) {
                        return e;
                    }
                    while ( ( e = e.parentNode ) && !!e.ownerDocument ) {
                        if ( e[ match ]( s ) ) {
                            return e;
                        }
                    }
                    return null;
                };
        } )() ,
        // unique identifier for all bubble
        _COUNTER = 0;
    
    function isset ( x ) {
        return x !== undefined;
    }
    
    function exist ( _ ) {
        return _ !== undefined && _ !== null;
    }
    
    function isfunction ( _ ) {
        return exist( _ ) && typeof _ === 'function';
    }
    
    var isLeftClick = ( function () {
        
        function isEvent ( event ) {
            if ( exist( event ) ) {
                return ( event instanceof Event ) ||
                    ( exist( event.defaultEvent ) && event.defaultEvent instanceof Event ) ||
                    ( exist( event.originalEvent ) && event.originalEvent instanceof Event );
            }
            return false;
        }
        
        function isTouch ( event ) {
            return event.type.charAt( 0 ) === 't';
        }
        
        function isClick ( event ) {
            return event.type == 'click';
        }
        
        function isMouse ( event ) {
            return event.type.slice( 0 , 5 ) == 'mouse';
        }
        
        function isWhich ( event , n ) {
            return isset( event.which ) && event.which == n;
        }
        
        function isButton ( event , n ) {
            return isset( event.button ) && event.button == n;
        }
        
        return function ( event ) {
            return isEvent( event ) && ( isTouch( event ) && isWhich( event , 0 ) ) ||
                ( ( isClick( event ) || isMouse( event ) ) && ( isWhich( event , 1 ) || isButton( event , 0 ) ) );
        };
        
    } )();
    
    /******************************************
     *                 OBSERVER               *
     ******************************************/
    var observer = window.MutationObserver || window.WebKitMutationObserver ,
        indexer = {};
    
    function inarray ( a , v ) {
        return a.indexOf( v ) >= 0;
    }
    
    function arrayUnique ( a ) {
        if ( Array.isArray( a ) ) {
            for ( var n = [] , i = 0 , l = a.length ; i < l ; i++ ) {
                !inarray( n , a[ i ] ) && n.push( a[ i ] );
            }
            return n;
        }
        return [];
    }
    
    function browseMutationRemovedNodes ( event ) {
        var removed = [] , j , m , a;
        
        if ( event ) {
            for ( var i = 0 , l = event.length ; i < l ; i++ ) {
                for ( j = 0, a = event[ i ].removedNodes, m = a.length ; j < m ; j++ ) {
                    removed.push( a[ j ] );
                }
            }
            
            removed = arrayUnique( removed );
            
            if ( removed.length ) {
                return removed;
            }
        }
        
        return null;
    }
    
    if ( observer ) {
        /**
         * standalone-document-ready
         * @license MIT
         * @see {@link https://framagit.org/tla/standalone-document-ready}
         * */
        var domLoad = window.domLoad = ( function () {
            var ready = false ,
                stackfunction = [];
            
            function stack ( callback ) {
                if ( isfunction( callback ) ) {
                    if ( ready ) {
                        return callback.call( document );
                    }
                    
                    stackfunction.push( callback );
                }
            }
            
            function load () {
                ready = true;
                
                stackfunction.forEach( function ( callback ) {
                    callback.call( document );
                } );
                
                stackfunction = null;
                
                document.removeEventListener( 'DOMContentLoaded' , load , true );
                document.removeEventListener( 'DomContentLoaded' , load , true );
            }
            
            document.addEventListener( 'DOMContentLoaded' , load , true );
            document.addEventListener( 'DomContentLoaded' , load , true );
            
            return stack;
        } )();
        
        domLoad( function () {
            observer = new observer( function ( e ) {
                var removed = browseMutationRemovedNodes( e ) ,
                    body = document.body ,
                    inst;
                
                if ( removed ) {
                    for ( var i in indexer ) {
                        inst = indexer[ i ];
                        
                        if ( !body.contains( inst.element ) ) {
                            inst.destroy();
                        }
                    }
                }
            } );
            
            observer.observe( document.body , {
                childList : true ,
                subtree : true
            } );
        } );
    }
    
    /******************************************
     *             CSS CONSTRUCTOR            *
     ******************************************/
    /**
     * css-rule
     * @license MIT
     * @see {@link https://framagit.org/tla/css-rule}
     * */
    var cssConstructor = ( function () {
        
        var passiveSupport = false;
        
        // check if the browser support passive events
        ( function () {
            var tmp = document.createDocumentFragment();
            
            var empty = function () {} , setting = {
                get passive () {
                    passiveSupport = true;
                    return false;
                }
            };
            
            tmp.addEventListener( 'click' , empty , setting );
            
            empty = tmp = null;
        } )();
        
        var passiveArgument = function ( c ) {
            if ( passiveSupport ) {
                return {
                    passive : true ,
                    capture : c
                };
            }
            
            return c;
        };
        
        function sniffer ( instance , callback ) {
            var self = this;
            
            var random = '_' + Math.random().toString( 30 ).substring( 2 );
            
            this.element = instance.document.createElement( 'div' );
            
            this.window = instance.document.defaultView;
            
            this.rule = '#' + random;
            
            this.css = instance;
            
            this.element.setAttribute( 'style' ,
                'pointer-events: none;' +
                'visibility: hidden' +
                'z-index: -1;' +
                
                'position: relative;' +
                'display: block;' +
                
                'height: 10px;' +
                'width: 10px;'
            );
            
            this.element.id = random;
            
            instance.document.body.appendChild( this.element );
            
            this.resize = this.onresize( this.element , function () {
                callback();
                
                self.destroy();
            } );
            
            this.css.add( this.rule , 'width: 20px !important; height: 20px !important;' );
        }
        
        sniffer.prototype.destroy = function () {
            if ( this.element.parentNode ) {
                this.element.parentNode.removeChild( this.element );
            }
            
            if ( this.resize ) {
                this.resize();
                this.resize = null;
            }
            
            this.css.remove( this.rule );
        };
        
        sniffer.prototype.onresize = ( function () {
            var generic = 'top: 0; left: 0; right: 0; bottom: 0; position: absolute; overflow: scroll;' ,
                style = {
                    scroller : generic ,
                    expand : generic + 'width: 20px; height: 20px;' ,
                    container : generic + 'visibility: hidden; pointer-events: none; z-index: -1;'
                };
            
            var pattern = ( function () {
                var container = document.createElement( 'DIV' ) ,
                    doc = document.createDocumentFragment();
                
                container.style.cssText = style.container;
                
                container.innerHTML = '' +
                    '<div style="' + style.scroller + '" >' +
                    '   <div style="' + style.expand + '" ></div>' +
                    '</div>';
                
                doc.appendChild( container );
                
                return doc;
            } )();
            
            function getsizeprop ( element , prop ) {
                return element[ 'client' + prop ] || element[ 'offset' + prop ] || 0;
            }
            
            function getsize ( element ) {
                return {
                    width : getsizeprop( element , 'Width' ) ,
                    height : getsizeprop( element , 'Height' )
                };
            }
            
            function reset ( scrollerExpand ) {
                scrollerExpand.scrollTop = scrollerExpand.scrollHeight;
            }
            
            return function ( element , callback ) {
                var container = pattern.cloneNode( true ) ,
                    current = getsize( element ) ,
                    window = this.window;
                
                function destroy () {
                    window.removeEventListener( 'scroll' , eventHandler , true );
                }
                
                function change () {
                    var tmp = getsize( element );
                    
                    if ( tmp.width != current.width || tmp.height != current.height ) {
                        destroy();
                        callback();
                    }
                }
                
                function eventHandler ( e ) {
                    if ( element.contains( e.target ) ) {
                        e.stopImmediatePropagation();
                        change();
                    }
                }
                
                element.appendChild( container );
                
                reset( element.children[ 0 ].children[ 0 ] );
                
                window.addEventListener( 'scroll' , eventHandler , passiveArgument( true ) );
                
                return destroy;
            };
            
        } )();
        
        function cssgen ( doc ) {
            !doc && ( doc = document );
            
            this.id = 0;
            this.index = {};
            this.timeout = [];
            this.document = doc;
            
            this.style = doc.createElement( 'STYLE' );
            this.style.setAttribute( 'type' , 'text/css' );
            this.style.appendChild( doc.createTextNode( "" ) );
            
            this.head = doc.getElementsByTagName( 'HEAD' )[ 0 ];
            this.head.appendChild( this.style );
            
            this.api = this.style.sheet;
            this.rules = this.api.cssRules;
            this.insertRule = 'insertRule' in this.api ? 'insertRule' : 'addRule';
            this.deleteRule = 'deleteRule' in this.api ? 'deleteRule' : 'removeRule';
        }
        
        cssgen.prototype.setTimeout = function ( fn , time ) {
            var id = setTimeout( function () { this.timeout.splice( this.timeout.indexOf( id ) , 1 ) , fn(); } , time );
            this.timeout.push( id );
        };
        
        cssgen.prototype.clearTimeout = function () {
            this.timeout.forEach( function ( id ) { clearTimeout( id ); } );
        };
        
        cssgen.prototype.removeByRule = function ( rule ) {
            var rules = Array.apply( null , this.rules ) , i;
            
            if ( ( i = rules.indexOf( rule ) , !!~i ) ) {
                this.api[ this.deleteRule ]( i );
                
                return true;
            }
            
            return false;
        };
        
        /**
         * @method cssgen.prototype.add
         * @param {string} selector
         * @param {string} rules
         * @param {function} [call]
         * @return {void}
         * */
        cssgen.prototype.add = function ( selector , rules , call ) {
            var rule , self = this;
            
            this.api[ this.insertRule ]( selector + '{' + rules + '}' , this.rules.length );
            
            rule = this.rules[ this.rules.length - 1 ];
            
            if ( !this.index[ selector ] ) {
                this.index[ selector ] = {
                    rules : [] ,
                    callbacks : {}
                };
            }
            
            this.index[ selector ].rules.push( rule );
            
            if ( isfunction( call ) ) {
                var id = ++this.id;
                
                this.index[ selector ].callbacks[ id ] = new sniffer( this , function () {
                    delete self.index[ selector ].callbacks[ id ];
                    call();
                } );
            }
        };
        
        /**
         * @method cssgen.prototype.remove
         * @param {string} selector
         * @return {boolean}
         * */
        cssgen.prototype.remove = function ( selector ) {
            var result = true ,
                callbacks , array , i , l;
            
            if ( array = this.index[ selector ] ) {
                for ( i = 0, l = array.length ; i < l ; ) {
                    if ( !this.removeByRule( array[ i++ ] ) ) {
                        result = false;
                    }
                }
                
                callbacks = this.index[ selector ].callbacks;
                
                for ( i in callbacks ) {
                    callbacks[ i ].destroy();
                }
                
                delete this.index[ selector ];
            }
            
            return result;
        };
        
        /**
         * @method cssgen.prototype.destroy
         * @return {void}
         * */
        cssgen.prototype.destroy = function () {
            this.clearTimeout();
            this.head.removeChild( this.style );
        };
        
        return cssgen;
        
    } )();
    
    /******************************************
     *                   FN                   *
     ******************************************/
    function toRatio ( n ) {
        return n / 100;
    }
    
    function transform ( n ) {
        return '-webkit-transform:' + n + ';' +
            'transform:' + n + ';';
    }
    
    function isDisabled ( element ) {
        return !!closest( element , '[disabled]' );
    }
    
    function scale ( n ) {
        return transform( 'scale(' + n + ')' );
    }
    
    function transition ( prop , val ) {
        return '-webkit-transition-' + prop + ':' + val + ';' +
            'transition-' + prop + ':' + val + ';';
    }
    
    function prevent ( e ) {
        e.preventDefault();
        e.stopImmediatePropagation();
    }
    
    // element size
    function getSize ( element ) {
        return {
            height : element.offsetHeight ,
            width : element.offsetWidth ,
            left : element.offsetLeft ,
            top : element.offsetTop
        };
    }
    
    function getScale ( element ) {
        var beMagicAndGFY = 0.015 ,
            tmp = window.getComputedStyle( element );
        
        tmp = ( tmp.getPropertyValue( '-webkit-transform' ) || tmp.getPropertyValue( 'transform' ) ).replace( /[\s\u00A0]+/g , '' );
        
        if ( /^(matrix)/.test( tmp ) ) {
            return 'scale(' + ( +( tmp.slice( 7 , tmp.indexOf( ',' ) ) ) + beMagicAndGFY ) + ')';
        }
        
        return 'scale(' + ( +( tmp.slice( 6 , -1 ) ) + beMagicAndGFY ) + ')';
    }
    
    function getTouch ( event ) {
        if ( event = event[ 'touches' ] || event[ 'changedTouches' ] ) {
            return event;
        }
        return [];
    }
    
    // get mouse ( or finger ) position in the document
    function clientData ( event ) {
        var a;
        return event.type.charAt( 0 ) !== 't' ? {
            x : event.clientX ,
            y : event.clientY
        } : ( a = getTouch( event ), {
            x : a[ a.length - 1 ].clientX ,
            y : a[ a.length - 1 ].clientY
        } );
    }
    
    // get current max size of origin element and the mouse ( or finger ) position in the element
    function getData ( button , event ) {
        var size = button.getBoundingClientRect() ,
            client = clientData( event ) ,
            data = {
                width : size.width ,
                height : size.height ,
                y : client.y - size.top ,
                x : client.x - size.left ,
                max : Math.ceil( Math.sqrt( Math.pow( size.width , 2 ) + Math.pow( size.height , 2 ) ) ) * 2
            };
        
        data.calcX = data.x - ( data.max / 2 );
        data.calcY = data.y - ( data.max / 2 );
        
        return data;
    }
    
    function createBubble ( cls , document ) {
        var bubbleContainer = document.createElement( 'div' ) ,
            bubbleDown = document.createElement( 'div' ) ,
            bubbleUp = document.createElement( 'div' );
        
        bubbleDown.classList.add( 'bubble-' + cls );
        bubbleUp.classList.add( 'bubble-' + cls );
        
        bubbleDown.setAttribute( 'identifier' , 'bubble-' + cls );
        
        bubbleContainer.appendChild( bubbleDown );
        bubbleContainer.appendChild( bubbleUp );
        
        return bubbleContainer;
    }
    
    // move and resize bubble container
    function moveContainer () {
        var comput = window.getComputedStyle( this.element ) ,
            prop = JSON.stringify( comput ) ,
            size;
        
        if ( ( this.lastCSS && this.lastCSS == prop ) && !this.forceMove ) {
            return;
        }
        
        this.lastCSS = prop;
        this.css.remove( '.' + this.className + ' > .awesome-bubble-container' );
        
        if ( { fixed : 1 , relative : 1 , absolute : 1 }[ trim( comput.getPropertyValue( 'position' ) ).toLowerCase() ] ) {
            return this.css.add( '.' + this.className + ' > .awesome-bubble-container' , 'top:0; left:0; width:100%; height:100%' );
        }
        
        size = getSize( this.element );
        
        this.css.add( '.' + this.className + ' > .awesome-bubble-container' , '' +
            'top:' + size.top + 'px;' +
            'left:' + size.left + 'px;' +
            'width:' + size.width + 'px;' +
            'height:' + size.height + 'px' );
    }
    
    function buildGenericCss ( document ) {
        if ( document.awesomeBubbleCssBuild ) {
            return;
        }
        
        var css = new cssConstructor( document );
        
        // *
        css.add( '.awesome-bubble-pattern > div.awesome-bubble-container ,' +
            '.awesome-bubble-pattern > div.awesome-bubble-container div' ,
            'border:none !important;' +
            'margin:0 !important;' +
            'padding:0 !important;' +
            'overflow:hidden !important;' +
            'pointer-events:none !important'
        );
        
        // bubble container
        css.add( '.awesome-bubble-pattern > div.awesome-bubble-container' , 'position:absolute;border-radius:inherit' );
        
        // bubble sub container
        css.add( '.awesome-bubble-pattern > div.awesome-bubble-container > div' ,
            transition( 'property' , 'opacity' ) +
            transition( 'timing-function' , 'ease-out' ) +
            'position:absolute;top:0;left:0;width:100%;height:100%' );
        css.add( '.awesome-bubble-pattern > div.awesome-bubble-container > div.awesome-bubble-remove' , 'opacity:0' );
        
        // bubble *
        css.add( '.awesome-bubble-pattern > div.awesome-bubble-container > div > div' ,
            transition( 'property' , 'transform , background-color' ) +
            transition( 'timing-function' , 'ease-out' ) +
            'border-radius:100%;position:absolute' );
        
        // bubble :first
        css.add( '.awesome-bubble-pattern > div.awesome-bubble-container > div > div:first-child' , scale( 0 ) );
        
        // bubble :last
        css.add( '.awesome-bubble-pattern > div.awesome-bubble-container > div > div:last-child' , 'display:none' );
        
        // bubble water
        css.add( '.awesome-bubble-pattern > div.awesome-bubble-container > div.awesome-bubble-water-remove' , 'opacity:0' );
        
        document.awesomeBubbleCssBuild = true, css = null;
    }
    
    /******************************************
     *              HYBRID EVENT              *
     ******************************************/
    var listen = [] ,
        handler = [] ,
        hackMouse = ( function () {
            var api = {};
            
            [ 'mousedown' , 'mousemove' , 'mouseup' ].forEach( function ( type ) {
                var cur = api[ type ] = {
                    auth : true ,
                    idTime : null ,
                    block : function ( safe ) {
                        cur.auth = false;
                        clearTimeout( cur.idTime );
                        cur.idTime = setTimeout( function () {
                            cur.auth = true;
                        } , safe ? 200 : 800 );
                    }
                };
            } );
            
            return {
                touch : function ( call , event ) {
                    switch ( event.type ) {
                        
                        case 'touchstart':
                            api.mousedown.block();
                            api.mousemove.block();
                            api.mouseup.block();
                            event.target.addEventListener( 'contextmenu' , prevent );
                            break;
                        
                        case 'touchmove':
                            api.mousemove.block();
                            break;
                        
                        case 'touchend':
                        case 'touchleave':
                        case 'touchcancel':
                            api.mouseup.block();
                            event.target.removeEventListener( 'contextmenu' , prevent );
                            break;
                        
                    }
                    
                    call.call( this , event );
                } ,
                
                mouse : function ( call , event ) {
                    if ( event.type !== 'mousemove' && !isLeftClick( event ) ) {
                        return;
                    }
                    switch ( event.type ) {
                        
                        case 'mouseup':
                            api.mousemove.block( true );
                            break;
                        
                    }
                    
                    if ( api[ event.type ].auth ) {
                        call.call( this , event );
                    }
                }
            };
        } )();
    
    function type ( type ) {
        var eTouch , eMouse;
        switch ( type ) {
            
            case 'down':
                eTouch = 'touchstart';
                eMouse = 'mousedown';
                break;
            
            case 'move':
                eTouch = 'touchmove';
                eMouse = 'mousemove';
                break;
            
            case 'up':
                eTouch = 'touchend,touchleave,touchcancel';
                eMouse = 'mouseup';
                break;
            
        }
        
        return { touch : eTouch , mouse : eMouse };
    }
    
    function on ( e , d , f , c ) {
        var mouse = hackMouse.mouse.bind( null , f ) ,
            touch = hackMouse.touch.bind( null , f );
        
        e = type( e );
        
        handler.push( f );
        listen.push( { touch : touch , mouse : mouse } );
        
        e.touch.split( ',' ).forEach( function ( e ) {
            d.addEventListener( e , touch , passiveEventHandler( c ) );
        } );
        d.addEventListener( e.mouse , mouse , passiveEventHandler( c ) );
    }
    
    function off ( e , d , f , c ) {
        var i = handler.indexOf( f ) , l;
        
        if ( i !== -1 ) {
            l = listen[ i ], e = type( e );
            
            handler.splice( i , 1 );
            listen.splice( i , 1 );
            
            e.touch.split( ',' ).forEach( function ( e ) {
                d.removeEventListener( e , l.touch , c );
            } );
            d.removeEventListener( e.mouse , l.mouse , c );
        }
    }
    
    function onDown ( d , f , c ) {
        on( 'down' , d , f , c );
    }
    
    function offDown ( d , f , c ) {
        off( 'down' , d , f , c );
    }
    
    function onMove ( d , f , c ) {
        on( 'move' , d , f , c );
    }
    
    function offMove ( d , f , c ) {
        off( 'move' , d , f , c );
    }
    
    function onUp ( d , f , c ) {
        on( 'up' , d , f , c );
    }
    
    function offUp ( d , f , c ) {
        off( 'up' , d , f , c );
    }
    
    /******************************************
     *          BUBBLE CONSTRUCTOR            *
     ******************************************/
    function _CONSTRUCT ( current , element , document ) {
        this.bubble = [];
        this.active = true;
        this.uniqBuble = 0;
        this.timeout = null;
        this.bubbleNumber = 0;
        this.bubbleNumberEnd = 0;
        this.forceMove = false;
        this.cleartimeout = null;
        this.forExpandBubble = [];
        this.element = element, element = null;
        this.css = new cssConstructor( document );
        this.container = document.createElement( 'div' );
        this.focuced = 'awesome-bubble-focuced-' + current;
        this.className = 'awesome-bubble-' + current, current = null;
        
        this.current = current;
        indexer[ current ] = this;
        
        // setting
        this.setting = {
            number : 1 ,
            opacity : 0.3 ,
            duration : 700 ,
            interval : 100 ,
            bubbleSize : 1 ,
            bubbleEffect : true ,
            
            waterEffect : false ,
            intervalWater : 100 ,
            waterDuration : 1000 ,
            bubbleWaterSize : 0.1 ,
            waterOnlyWhenPress : false ,
            
            material : false ,
            stayOnFocus : false ,
            
            colorEnd : [] ,
            color : [ 'cornflowerblue' ]
        };
        
        this.onresize = function () { this.forceMove = true; }.bind( this );
        window.addEventListener( 'resize' , this.onresize , passiveEventHandler( true ) );
        
        // on put down ( mousedown , touchstart )
        this.putDown = function ( e ) {
            var i , data ,
                reflow , loop ,
                fake = e.type == 'fake' ,
                bubble = this.setting.bubbleEffect ,
                stayonfocus = this.setting.stayOnFocus && bubble ,
                material = this.setting.material && !this.setting.stayOnFocus;
            
            if ( isDisabled( this.element ) || ( stayonfocus && this.element.classList.contains( this.focuced ) ) ) {
                return;
            }
            
            !e && ( e = window.event );
            data = getData( this.element , e );
            
            if ( data.x >= 0 && data.y >= 0 && data.x <= data.width && data.y <= data.height ) {
                
                clearTimeout( this.timeout );
                clearTimeout( this.cleartimeout );
                
                if ( !stayonfocus && !fake ) {
                    onUp( document , this.putUp , true );
                }
                
                if ( stayonfocus ) {
                    this.element.classList.add( this.focuced );
                    onDown( document , this.putOut , true );
                }
                else if ( this.setting.waterEffect && this.setting.waterOnlyWhenPress ) {
                    onMove( this.element , this.putMove , true );
                }
                
                if ( bubble ) {
                    i = 0;
                    
                    reflow = function ( d ) {
                        var bubble = this.reflow( data , d );
                        bubble.firstElementChild.classList.add( material ? 'material' : 'basic' );
                        loop();
                    }.bind( this );
                    
                    loop = function () {
                        if ( this.setting.number > ++i ) {
                            reflow( this.setting.interval * i );
                        }
                        else {
                            if ( !this.setting.stayOnFocus && !this.setting.waterEffect ) {
                                if ( fake ) {
                                    return this.cleartimeout = setTimeout( this.putUp , 100 );
                                }
                                
                                this.cleartimeout = setTimeout( this.putUp , this.setting.duration * ( material ? 10.3 : 2 ) );
                            }
                        }
                    }.bind( this );
                    
                    // ensure the bubble container is the last child
                    this.container.nextSibling && this.element.appendChild( this.container );
                    
                    moveContainer.call( this );
                    
                    reflow( 0 );
                }
                
            }
        }.bind( this );
        
        // water effect
        this.water = [];
        this.putMoveBlocker = false;
        this.putMoveBlockerTimeout = null;
        this.putMove = function ( e ) {
            var water = {};
            
            if ( !isDisabled( this.element ) ) {
                
                if ( !this.putMoveBlocker ) {
                    this.putMoveBlocker = true;
                    moveContainer.call( this );
                    !e && ( e = window.event );
                    
                    this.reflow( getData( this.element , e ) , false , true , function ( bubble ) {
                        var child = bubble.firstElementChild ,
                            id = child.getAttribute( 'identifier' );
                        
                        water.bubble = bubble;
                        
                        child.classList.add( 'awesome-bubble-water-expand' );
                        bubble.classList.add( 'awesome-bubble-water-remove' );
                        
                        water.timeout = setTimeout( function () {
                            
                            this.container.removeChild( bubble );
                            
                            this.css.remove( '.' + this.className + ' > .awesome-bubble-container > div > div.' + id );
                            this.css.remove( '.' + this.className + ' > .awesome-bubble-container > div > div.' + id + '.awesome-bubble-color-end' );
                            
                            this.water.splice( this.water.indexOf( water ) , 1 ), bubble = water = null;
                            
                        }.bind( this ) , this.setting.waterDuration * 2 );
                    }.bind( this ) );
                    
                    this.water.push( water );
                    
                    this.putMoveBlockerTimeout = setTimeout( function () {
                        this.putMoveBlocker = false;
                    }.bind( this ) , this.setting.intervalWater );
                }
                
            }
        }.bind( this );
        
        // on put up ( mouseup , touchend )
        this.putUp = function () {
            var material = this.setting.material && !this.setting.stayOnFocus;
            
            clearTimeout( this.timeout );
            clearTimeout( this.cleartimeout );
            
            if ( this.setting.waterEffect && this.setting.waterOnlyWhenPress ) {
                this.putMoveBlocker = false;
                clearTimeout( this.putMoveBlockerTimeout );
                offMove( this.element , this.putMove , true );
            }
            
            offUp( document , this.putUp , true );
            
            // acceleration de la bulle ( explosion )
            if ( this.bubble.length ) {
                this.forExpandBubble.forEach( function ( bubbleContainer , index ) {
                    
                    var bubbleDown = bubbleContainer.firstElementChild ,
                        bubbleUp = bubbleContainer.lastElementChild ,
                        cls = bubbleDown.getAttribute( 'identifier' );
                    
                    if ( !material ) {
                        bubbleContainer.classList.add( 'awesome-bubble-remove' );
                        bubbleUp.classList.add( 'awesome-bubble-expand' );
                        return;
                    }
                    
                    this.css.add( '.awesome-bubble-pattern > div.awesome-bubble-container > div > div:last-child.' + cls ,
                        transform( getScale( bubbleDown ) ) ,
                        function () {
                            
                            bubbleDown.style.display = 'none';
                            bubbleUp.style.display = 'block';
                            
                            this.css.add( '.awesome-bubble-pattern > div.awesome-bubble-container > div > div:last-child.' + cls + '.awesome-bubble-expand' ,
                                scale( this.setting.bubbleSize ) +
                                transition( 'duration' , this.setting.duration + 'ms' ) ,
                                function () {
                                    
                                    bubbleContainer.classList.add( 'awesome-bubble-remove' );
                                    bubbleUp.classList.add( 'awesome-bubble-expand' );
                                    
                                } );
                            
                        }.bind( this ) );
                    
                    this.forExpandBubble.splice( index , 1 );
                    
                }.bind( this ) );
            }
            
            // when the bubble is fade, clear all
            this.timeout = setTimeout( this.clear.bind( this ) , this.setting.duration * 2 );
        }.bind( this );
        
        this.putOut = function ( e , f ) {
            if ( ( f && this.element.classList.contains( this.focuced ) ) || ( e && !closest( e.target , '.' + this.focuced ) ) ) {
                offDown( document , this.putOut , true );
                this.element.classList.remove( this.focuced );
                this.putUp();
            }
        }.bind( this );
    }
    
    // clear class and custom css
    _CONSTRUCT.prototype.clear = function ( hard ) {
        var length , remove , id;
        if ( ( length = this.bubble.length ) ) {
            while ( ( remove = this.bubble[ --length ] ) ) {
                this.container.removeChild( remove );
                if ( !hard ) {
                    id = remove.firstElementChild.getAttribute( 'identifier' );
                    this.css.remove( '.' + this.className + ' > .awesome-bubble-container > div > div.' + id );
                    this.css.remove( '.awesome-bubble-pattern > div.awesome-bubble-container > div > div:last-child.' + id );
                    this.css.remove( '.' + this.className + ' > .awesome-bubble-container > div > div.' + id + '.awesome-bubble-color-end' );
                    this.css.remove( '.awesome-bubble-pattern > div.awesome-bubble-container > div > div:last-child.' + id + '.awesome-bubble-expand' );
                }
            }
            this.uniqBuble = this.bubbleNumber = 0;
            this.bubble = [];
        }
    };
    
    // resize bubble and bubble container, and start it
    _CONSTRUCT.prototype.reflow = function ( data , delay , water , call ) {
        var self = this ,
            colorEnd = this.setting.colorEnd.length ,
            bubble = createBubble( ++this.uniqBuble , document );
        
        function ready () {
            var bubbleDown = bubble.firstElementChild;
            
            if ( self.setting.colorEnd.length ) {
                bubbleDown.classList.add( 'awesome-bubble-color-end' );
            }
            
            if ( water ) {
                return call( bubble );
            }
            
            bubbleDown.classList.add( 'awesome-bubble-expand' );
        }
        
        // bubble position ( the mouse cursor ) and size
        this.css.add( '.' + this.className + ' > .awesome-bubble-container > div > div.bubble-' + this.uniqBuble ,
            'top:' + data.calcY + 'px;' +
            'left:' + data.calcX + 'px;' +
            'width:' + data.max + 'px;' +
            'height:' + data.max + 'px;' +
            ( delay ? transition( 'delay' , delay + 'ms' ) : '' ) +
            'background-color:' + ( this.bubbleNumber >= this.setting.color.length &&
            ( this.bubbleNumber = 0 ), this.setting.color[ this.bubbleNumber++ ] ) ,
            !colorEnd ? ready : null );
        
        if ( colorEnd ) {
            this.css.add(
                // bubble
                '.' + this.className + ' > .awesome-bubble-container > div > ' +
                'div.bubble-' + this.uniqBuble + '.awesome-bubble-color-end' ,
                
                // background
                'background-color:' + ( this.bubbleNumberEnd >= this.setting.colorEnd.length &&
                ( this.bubbleNumberEnd = 0 ), this.setting.colorEnd[ this.bubbleNumberEnd++ ] ) ,
                
                // callback
                ready
            );
        }
        
        if ( !water ) {
            this.bubble.push( bubble );
            this.forExpandBubble.push( bubble );
        }
        
        this.container.appendChild( bubble );
        
        return bubble;
    };
    
    _CONSTRUCT.prototype.clearwater = function () {
        this.water.forEach( function ( water ) {
            var id = water.bubble.firstElementChild.getAttribute( 'identifier' );
            clearTimeout( water.timeout );
            this.container.removeChild( water.bubble );
            this.css.remove( '.' + this.className + ' > .awesome-bubble-container > div > div.' + id );
            this.css.remove( '.' + this.className + ' > .awesome-bubble-container > div > div.' + id + '.awesome-bubble-color-end' );
        }.bind( this ) );
        this.water = [];
        clearTimeout( this.putMoveBlockerTimeout );
        offMove( this.element , this.putMove , true );
    };
    
    _CONSTRUCT.prototype.disable = function () {
        if ( this.active ) {
            this.active = false;
            offDown( this.element , this.putDown , true );
            this.setting.waterEffect && offMove( this.element , this.putMove , true );
        }
    };
    
    _CONSTRUCT.prototype.enable = function () {
        if ( !this.active ) {
            this.active = true;
            onDown( this.element , this.putDown , true );
            ( this.setting.waterEffect && !this.setting.waterOnlyWhenPress ) && onMove( this.element , this.putMove , true );
        }
    };
    
    _CONSTRUCT.prototype.emulate = function () {
        var d;
        if ( this.active ) {
            d = this.element.getBoundingClientRect();
            
            return this.putDown( {
                type : 'fake' ,
                clientX : Math.round( d.left + ( d.width / 2 ) ) ,
                clientY : Math.round( d.top + ( d.height / 2 ) )
            } );
        }
    };
    
    // kill _awesomeBubble instance
    _CONSTRUCT.prototype.destroy = function () {
        delete indexer[ this.current ];
        
        // clearTimeout
        clearTimeout( this.timeout );
        clearTimeout( this.cleartimeout );
        
        // remove element
        this.clearwater();
        this.clear( true );
        this.element.classList.remove( this.className );
        this.element.classList.remove( 'awesome-bubble-pattern' );
        this.container.parentNode && this.container.parentNode.removeChild( this.container );
        
        // remove css rules
        if ( this.css ) {
            this.css.destroy();
            this.css = null;
        }
        
        // remove event
        offUp( document , this.putUp , true );
        offMove( this.element , this.putMove , true );
        offDown( this.element , this.putDown , true );
        window.removeEventListener( 'resize' , this.onresize , true );
    };
    
    var validate = ( function () {
        var minint = 30;
        
        function bool ( defaultValue , newValue ) {
            if ( isset( newValue ) ) {
                return !!newValue;
            }
            
            return defaultValue;
        }
        
        function ratio ( defaultValue , newValue ) {
            if ( isset( newValue ) ) {
                newValue = parseFloat( newValue );
                
                if ( !isNaN( newValue ) ) {
                    return toRatio( newValue );
                }
            }
            
            return defaultValue;
        }
        
        function int ( defaultValue , newValue ) {
            if ( isset( newValue ) ) {
                newValue = parseInt( newValue );
                
                if ( !isNaN( newValue ) ) {
                    return newValue < 1 ? 1 : newValue;
                }
            }
            
            return defaultValue;
        }
        
        function minInt ( defaultValue , newValue ) {
            if ( isset( newValue ) ) {
                newValue = parseInt( newValue );
                
                if ( !isNaN( newValue ) ) {
                    return newValue < minint ? minint : newValue;
                }
            }
            
            return defaultValue;
        }
        
        function validString ( val ) {
            return typeof val == 'string' && !!val.trim();
        }
        
        function array ( defaultValue , newValue ) {
            if ( Array.isArray( newValue ) ) {
                return newValue.filter( validString );
            }
            
            return validString( newValue ) ? [ newValue ] : defaultValue;
        }
        
        return {
            
            material : bool ,
            stayOnFocus : bool ,
            waterEffect : bool ,
            bubbleEffect : bool ,
            waterOnlyWhenPress : bool ,
            
            opacity : ratio ,
            bubbleSize : ratio ,
            bubbleWaterSize : ratio ,
            
            duration : minInt ,
            interval : minInt ,
            intervalWater : minInt ,
            waterDuration : minInt ,
            
            color : array ,
            colorEnd : array ,
            
            number : int
            
        };
        
    } )();
    
    _CONSTRUCT.prototype.setParams = function ( name , value , init ) {
        this.setting[ name ] = validate[ name ]( this.setting[ name ] , value );
        
        if ( !init && this[ 'setStylesheet_' + name ] ) {
            this[ 'setStylesheet_' + name ]();
        }
    };
    
    _CONSTRUCT.prototype.setStylesheet_duration = function () {
        var tmp;
        
        tmp = '.' + this.className + ' > .awesome-bubble-container > div > div.material:first-child';
        this.css.remove( tmp );
        this.css.add( tmp , transition( 'duration' , ( this.setting.duration * 10 ) + 'ms' ) );
        
        tmp = '.' + this.className + ' > .awesome-bubble-container > div > div.basic:first-child';
        this.css.remove( tmp );
        this.css.add( tmp , transition( 'duration' , this.setting.duration + 'ms' ) );
        
        tmp = '.' + this.className + ' > .awesome-bubble-container > div.awesome-bubble-remove';
        this.css.remove( tmp );
        this.css.add( tmp , transition( 'duration' , this.setting.duration + 'ms' ) );
    };
    
    _CONSTRUCT.prototype.setStylesheet_waterDuration = function () {
        var tmp;
        
        tmp = '.' + this.className + ' > .awesome-bubble-container > div > div.awesome-bubble-water-expand';
        this.css.remove( tmp );
        this.css.add( tmp , transition( 'duration' , this.setting.waterDuration + 'ms' ) );
        
        tmp = '.' + this.className + ' > .awesome-bubble-container > div.awesome-bubble-water-remove';
        this.css.remove( tmp );
        this.css.add( tmp , transition( 'duration' , this.setting.waterDuration + 'ms' ) );
    };
    
    _CONSTRUCT.prototype.setStylesheet_opacity = function () {
        var tmp;
        
        tmp = '.' + this.className + ' > div.awesome-bubble-container > div > div';
        this.css.remove( tmp );
        this.css.add( tmp , 'opacity:' + this.setting.opacity );
    };
    
    _CONSTRUCT.prototype.setStylesheet_bubbleSize = function () {
        var tmp;
        
        tmp = '.' + this.className + ' > div.awesome-bubble-container > div > div:first-child.awesome-bubble-expand';
        this.css.remove( tmp );
        this.css.add( tmp , scale( this.setting.bubbleSize ) );
    };
    
    _CONSTRUCT.prototype.setStylesheet_bubbleWaterSize = function () {
        var tmp;
        
        tmp = '.' + this.className + ' > div.awesome-bubble-container > div > div:first-child.awesome-bubble-water-expand';
        this.css.remove( tmp );
        this.css.add( tmp , scale( this.setting.bubbleWaterSize ) );
    };
    
    
    /******************************************
     *                 CALLER                 *
     ******************************************/
    window._awesomeBubble = function ( element , setting ) {
        var doc , self;
        
        if ( element.classList.contains( 'awesome-bubble-pattern' ) ) {
            return;
        }
        
        doc = setting && setting.document ? setting.document : document;
        self = new _CONSTRUCT( ++_COUNTER , element , doc );
        
        buildGenericCss( doc );
        
        // add class
        self.container.classList.add( 'awesome-bubble-container' );
        self.element.classList.add( 'awesome-bubble-pattern' );
        self.element.classList.add( self.className );
        
        // get option
        if ( setting ) {
            // validate settings
            for ( var i in validate ) {
                self.setParams( i , setting[ i ] , true );
            }
        }
        
        [
            'opacity' ,
            'duration' ,
            'bubbleSize' ,
            'waterDuration' ,
            'bubbleWaterSize'
        ].forEach( function ( name ) {
            self[ 'setStylesheet_' + name ]();
        } );
        
        // append element
        self.element.appendChild( self.container );
        
        // listen event
        onDown( self.element , self.putDown , true );
        
        if ( self.setting.waterEffect && !self.setting.waterOnlyWhenPress && !self.setting.stayOnFocus ) {
            onMove( self.element , self.putMove , true );
        }
        
        return element = setting = null, self;
    };
    
} )();

