( function () {
    
    var floor = Math.floor ,
        ceil = Math.ceil ,
        sqrt = Math.sqrt ,
        max = Math.max ,
        pow = Math.pow ,
        abs = Math.abs ,
        PI = Math.PI;
    
    function rangeRGB ( percent ) {
        var gotoR , gotoG , gotoB , between , t;
        
        if ( t = rangeCache[ ' ' + percent ] ) {
            return t;
        }
        
        if ( percent <= 0 ) {
            return rgb;
        }
        if ( percent >= 100 ) {
            return gotoRGB;
        }
        
        // R
        between = rgb[ 0 ] - gotoRGB[ 0 ];
        gotoR = ( abs( between ) / 100 ) * percent;
        between > 0 && ( gotoR = gotoR * -1 );
        
        // G
        between = rgb[ 1 ] - gotoRGB[ 1 ];
        gotoG = ( abs( between ) / 100 ) * percent;
        between > 0 && ( gotoG = gotoG * -1 );
        
        // B
        between = rgb[ 2 ] - gotoRGB[ 2 ];
        gotoB = ( abs( between ) / 100 ) * percent;
        between > 0 && ( gotoB = gotoB * -1 );
        
        t = [
            Math.round( parseInt( rgb[ 0 ] + gotoR ) ) ,
            Math.round( parseInt( rgb[ 1 ] + gotoG ) ) ,
            Math.round( parseInt( rgb[ 2 ] + gotoB ) )
        ];
        
        return rangeCache( percent , t ) , t;
    }
    
    var fbc ,
        type = 'bar' ,
        //type = 'wave' ,
        canvas = document.getElementById( "display" ) ,
        ctx = canvas.getContext( "2d" ) ,
        
        frame = window.requestAnimationFrame ||
            window.webkitRequestAnimationFrame ||
            window.mozRequestAnimationFrame ||
            window.msRequestAnimationFrame ||
            function ( c ) { return setTimeout( c , 16 ); } ,
        
        context = ( function () {
            var c = new ( window.AudioContext || window.webkitAudioContext )() ,
                a = c.createAnalyser() ,
                s = c.createMediaElementSource( Music.element );
            
            return s.connect( a ) , s.connect( c.destination ) , a;
        } )() ,
        
        getArray = function () {
            return ( fbc = new Uint8Array( 1024 ) ), context.getByteFrequencyData( fbc ) , fbc;
        } ,
        
        breakArray = function ( array , want ) {
            var l = array.length , interval = floor( array.length / want ) , r = [] , i = 0 , c = 0;
            for ( ; i < l && c < want ; r.push( array[ i += interval || 0 ] ), c++ ) {
                ;
            }
            return r;
        } ,
        
        deg = function ( d ) { return d * ( PI / 180 ); } ,
        
        draw = {
            
            'bar' : function ( data , goto ) {
                
                var bottom = 297 , left = 0 , i = 0 , total = 500 , range;
                
                var height = 4 ,
                    width = canvas.width / total;
                
                for ( data = data.slice( 0 , total ) ; i < total ; i++ ) {
                    range = rangeRGB( ( ( data[ i ] / goto ) * 100 ) || 0 );
                    ctx.fillStyle = "rgb(" + range[ 0 ] + "," + range[ 1 ] + "," + range[ 2 ] + ")";
                    ctx.fillRect( left += width , bottom - ( data[ i ] / 1 ) , width , height );
                }
                
            } ,
            
            'mosaic' : function ( data , goto ) {
                
                var size = 20 , i = 0 , total = 600 , countLeft = 0 , left = 10 , top = 290 , percent , range , v;
                
                for ( ; i < total ; i++ ) {
                    
                    percent = ( ( data[ i ] / 1.2 ) * 100 ) / goto;
                    v = ( ( ( size / 3 ) / 100 ) * percent );
                    
                    range = rangeRGB( ( ( data[ i ] / goto ) * 100 ) || 0 );
                    ctx.fillStyle = "rgb(" + range[ 0 ] + "," + range[ 1 ] + "," + range[ 2 ] + ")";
                    
                    ctx.beginPath() , ctx.arc( left , top , v || 0 , 0 , 2 * PI ) , ctx.fill();
                    
                    left += size;
                    
                    if ( ++countLeft >= 40 ) {
                        countLeft = 0;
                        top -= size;
                        left = 10;
                    }
                    
                }
                
            } ,
            
            'horizon' : function ( data , goto ) {
                
                var bottom = 150 , size = 7 , block = 8 ,
                    left = 0 , i = 0 , total = 200 , range ,
                    hauteur , j , c , Rleft = left , blockValue;
                
                for ( data = breakArray( data , 400 ) ; i < total ; i += 2 ) {
                    j = c = 0;
                    hauteur = ( data[ i ] / 1.8 );
                    blockValue = ceil( data[ i ] / ceil( hauteur / block ) );
                    
                    for ( ; j < hauteur ; j += block , c += blockValue ) {
                        range = rangeRGB( ( ( c / goto ) * 100 ) || 0 );
                        ctx.fillStyle = "rgb(" + range[ 0 ] + "," + range[ 1 ] + "," + range[ 2 ] + ")";
                        ctx.fillRect( Rleft , bottom - j , size , size );
                        ctx.fillRect( Rleft , bottom + j , size , size );
                    }
                    
                    Rleft = left += block;
                }
                
            } ,
            
            'circle' : function ( data , goto ) {
                
                var i = 0 , total = 360 , rotate = 0 , range , t;
                
                for ( ctx.save() , ctx.translate( 400 , 150 ) , data = breakArray( data , 800 ) ; i < total ; i += 2 , rotate += 2 ) {
                    t = data[ i ] / 8;
                    range = rangeRGB( ( ( data[ i ] / goto ) * 100 ) || 0 );
                    ctx.strokeStyle = "rgb(" + range[ 0 ] + "," + range[ 1 ] + "," + range[ 2 ] + ")";
                    
                    ctx.rotate( deg( rotate ) );
                    
                    ctx.beginPath() , ctx.moveTo( 0 , 115 ) , ctx.lineTo( 0 , 113 - t ) , ctx.stroke();
                    ctx.beginPath() , ctx.moveTo( 0 , 118 ) , ctx.lineTo( 0 , 120 + t ) , ctx.stroke();
                    
                    ctx.rotate( deg( rotate * -1 ) );
                    
                    if ( !~( i / 8 ).toString().indexOf( '.' ) ) {
                        
                        t = ( data[ i ] / 8 ) / 3;
                        
                        ctx.rotate( deg( rotate ) );
                        
                        ctx.beginPath() , ctx.moveTo( 0 , 20 ) , ctx.lineTo( 0 , 18 - t ) , ctx.stroke();
                        ctx.beginPath() , ctx.moveTo( 0 , 22 ) , ctx.lineTo( 0 , 24 + t ) , ctx.stroke();
                        
                        ctx.rotate( deg( rotate * -1 ) );
                    }
                }
                
                ctx.restore();
                
            } ,
            
            'building' : ( function () {
                
                function radius ( x1 , x2 , y1 , y2 ) {
                    return sqrt( pow( abs( x1 - x2 ) , 2 ) + pow( abs( y1 - y2 ) , 2 ) );
                }
                
                return function ( data , goto ) {
                    
                    var topX = 400 ,
                        topY = 100;
                    
                    var rightX = 600 ,
                        rightY = 200;
                    
                    var bottomX = 400 ,
                        bottomY = 300;
                    
                    var leftX = 200 ,
                        leftY = 200;
                    
                    var topright = radius( topX , rightX , topY , rightY ) ,
                        widthright = topright / 10;
                    
                    var topbottom = abs( bottomY - topY ) ,
                        heightbottom = topbottom / 10;
                    
                    var topleft = radius( topX , leftX , topY , leftY ) ,
                        widthleft = topleft / 10;
                    
                    var x = topX ,
                        y = topY ,
                        ligne = 0 ,
                        colonne = 0;
                    
                    for ( var i = 0 , l = 400 , gradient , range , tmpY ; i < l ; i += 4 ) {
                        
                        range = rangeRGB( ( ( data[ i ] / goto ) * 100 ) || 0 );
                        ctx.fillStyle = "rgb(" + range[ 0 ] + "," + range[ 1 ] + "," + range[ 2 ] + ")";
                        
                        tmpY = y - ( data[ i ] / 2.5 );
                        
                        // losange
                        // ---------------------------------
                        {
                            ctx.beginPath();
                            
                            // sommet
                            ctx.moveTo( x , tmpY );
                            
                            // point droit
                            ctx.lineTo( x + widthright , tmpY + ( heightbottom / 2 ) );
                            
                            // pied
                            ctx.lineTo( x , tmpY + heightbottom );
                            
                            // point gauche
                            ctx.lineTo( x - widthleft , tmpY + ( heightbottom / 2 ) );
                            
                            ctx.fill();
                        }
                        
                        // création du dégradé
                        gradient = ctx.createLinearGradient(
                            x - widthleft , tmpY + ( heightbottom / 2 ) , // debut = x,y sommet de la tour
                            x , y + heightbottom // fin = x,y pied de la tour
                        );
                        
                        gradient.addColorStop( 0 , "rgb(" + range[ 0 ] + "," + range[ 1 ] + "," + range[ 2 ] + ")" );
                        gradient.addColorStop( 1 , "rgb(" + rgb[ 0 ] + "," + rgb[ 1 ] + "," + rgb[ 2 ] + ")" );
                        ctx.fillStyle = gradient;
                        
                        // tour gauche
                        // ---------------------------------
                        {
                            ctx.beginPath();
                            
                            // toit gauche
                            ctx.moveTo( x - widthleft , tmpY + ( heightbottom / 2 ) );
                            
                            // toit droit
                            ctx.lineTo( x , tmpY + heightbottom );
                            
                            // pied droit
                            ctx.lineTo( x , y + heightbottom );
                            
                            // pied gauche
                            ctx.lineTo( x - widthleft , y + ( heightbottom / 2 ) );
                            
                            ctx.fill();
                        }
                        
                        // tour droite
                        // ---------------------------------
                        {
                            ctx.beginPath();
                            
                            // toit gauche
                            ctx.moveTo( x , tmpY + heightbottom );
                            
                            // toit droit
                            ctx.lineTo( x + widthright , tmpY + ( heightbottom / 2 ) );
                            
                            // pied droit
                            ctx.lineTo( x + widthright , y + ( heightbottom / 2 ) );
                            
                            // pied gauche
                            ctx.lineTo( x , y + heightbottom );
                            
                            ctx.fill();
                        }
                        
                        x += widthright;
                        y += ( heightbottom / 2 );
                        
                        if ( ++colonne == 10 ) {
                            colonne = 0;
                            ligne++;
                            
                            y = topY + ( ( heightbottom / 2 ) * ligne );
                            x = topX - ( widthleft * ligne );
                        }
                        
                    }
                    
                };
                
            } )() ,
            
            'eyes' : function ( data ) {
                var size = 50 ,
                    margSize = 130 ,
                    
                    top = 150 ,
                    Lleft = 195 ,
                    Rleft = Lleft + 400;
                
                function arc ( x , y , size ) {
                    ctx.beginPath();
                    ctx.arc( x , y , size , 0 , 2 * PI );
                    ctx.stroke();
                }
                
                ctx.lineWidth = 2;
                ctx.fillStyle = "black";
                ctx.strokeStyle = "white";
                
                // oeil gauche
                arc( Lleft , top , size );
                
                // oeil droit
                arc( Rleft , top , size );
                
                
                // ----------------------------
                // ----------------------------
                ctx.lineWidth = 5;
                data = data.slice( 0 , 100 );
                
                var i = 0 ,
                    width = 360 / 50 ,
                    rotate = 0 , t;
                
                // oeil gauche
                // ------------
                
                // point d'origne = centre de l'oeil
                ctx.save();
                ctx.translate( Lleft , top );
                ctx.fillStyle = "rgb(" + rgb[ 0 ] + "," + rgb[ 1 ] + "," + rgb[ 2 ] + ")";
                ctx.strokeStyle = "rgb(" + rgb[ 0 ] + "," + rgb[ 1 ] + "," + rgb[ 2 ] + ")";
                
                ctx.beginPath();
                ctx.moveTo( 0 , size - 7 );
                
                for ( ; i < 50 ; i++ , rotate += width ) {
                    t = data[ i ] / 20;
                    
                    ctx.rotate( deg( rotate ) );
                    
                    ctx.lineTo( 0 , ( size - 7 ) - t );
                    
                    ctx.rotate( deg( rotate * -1 ) );
                }
                
                ctx.lineTo( 0 , size - 7 );
                ctx.stroke();
                
                ctx.restore();
                
                // oeil droit
                // ------------
                
                // point d'origne = centre de l'oeil
                ctx.save();
                ctx.translate( Rleft , top );
                ctx.fillStyle = "rgb(" + gotoRGB[ 0 ] + "," + gotoRGB[ 1 ] + "," + gotoRGB[ 2 ] + ")";
                
                for ( ; i < 100 ; i++ , rotate += width ) {
                    t = data[ i ] / 20;
                    
                    ctx.rotate( deg( rotate ) );
                    
                    ctx.fillRect( 0 , size - 11 - t , ( 360 / 50 ) , 5 + t );
                    
                    ctx.rotate( deg( rotate * -1 ) );
                }
                
                ctx.restore();
                
                // ----------------------------
                // ----------------------------
                
                ctx.beginPath();
                ctx.lineWidth = 2;
                ctx.fillStyle = "black";
                ctx.strokeStyle = "white";
                
                // surcil gauche
                ctx.moveTo( Lleft - ( margSize + size ) , 80 );
                ctx.quadraticCurveTo( Lleft , 70 , Lleft + size + margSize , 200 );
                ctx.stroke();
                
                // sourcil gauche
                ctx.moveTo( Lleft - margSize , 80 );
                ctx.quadraticCurveTo( Lleft - size , 250 , Lleft + ( margSize + 13 ) , 175 );
                ctx.stroke();
                
                // surcil droit
                ctx.moveTo( Rleft - ( margSize + size ) , 200 );
                ctx.quadraticCurveTo( Rleft , 70 , Rleft + size + margSize , 80 );
                ctx.stroke();
                
                // sourcil droit
                ctx.moveTo( Rleft - ( margSize + 13 ) , 175 );
                ctx.quadraticCurveTo( Rleft + size , 250 , Rleft + margSize + 10 , 80 );
                ctx.stroke();
                
                // ----------------------------
                // ----------------------------
                
                ctx.beginPath();
                ctx.lineWidth = 10;
                ctx.strokeStyle = 'black';
                
                // masque surcil gauche
                ctx.moveTo( Lleft - ( margSize + size ) , 74 );
                ctx.quadraticCurveTo( Lleft , 64 , Lleft + size + margSize , 193 );
                ctx.stroke();
                
                // masque surcil droit
                ctx.moveTo( Rleft - ( margSize + size ) , 193 );
                ctx.quadraticCurveTo( Rleft , 64 , Rleft + size + margSize , 74 );
                ctx.stroke();
                
                // masque sourcil gauche
                ctx.moveTo( Lleft - margSize , 93 );
                ctx.quadraticCurveTo( Lleft - size , 253 , Lleft + ( margSize + 13 ) , 182 );
                ctx.stroke();
                
                // masque sourcil droit
                ctx.moveTo( Rleft - ( margSize + 13 ) , 182 );
                ctx.quadraticCurveTo( Rleft + size , 253 , Rleft + margSize + 10 , 92 );
                ctx.stroke();
                
                ctx.lineWidth = 2;
                
            } ,
            
            'dust' : ( function () {
                
                var dotlist = [] ,
                    last = Date.now() , diff = 30;
                
                // point de départ du sable
                var zero = {
                    left : canvas.width ,
                    top : ( ( 35 / 100 ) * canvas.height )
                };
                
                // marge d'erreur du point de départ et des trajectoires
                var around = 10 ,
                    aroundZero = ( around / 100 ) * canvas.height ,
                    betweenZeroTop = [ zero.top - aroundZero , zero.top + aroundZero ];
                
                // durer de l'animation de droite à gauche
                var duration = 15 * 1000;
                
                // emplacement top des étapes
                // TODO nombre d'étape dynamique ?
                var steps = {
                    first : {
                        left : 33 ,
                        top : 90
                    } ,
                    second : {
                        left : 66 ,
                        top : 30
                    } ,
                    third : {
                        left : 100 ,
                        top : 70
                    }
                };
                
                function between ( min , max ) {
                    return Math.floor( Math.random() * ( max - min + 1 ) + min );
                }
                
                function calcFrame ( manytime , begin , end ) {
                    var counter = manytime / ( 1000 / 60 );
                    return Math.abs( begin - end ) / counter;
                }
                
                function getFrame ( dot , step ) {
                    var tmp = steps[ step ];
                    
                    var top = between( tmp.top - around , tmp.top + around );
                    
                    var pxtop = ( ( top / 100 ) * canvas.height );
                    
                    dot.frame[ step ] = calcFrame( duration / 3 /* TODO calculer le temps en fonction de la distance */ , dot.vTop , pxtop );
                }
                
                function moveTop ( dot , percent , spectre ) {
                    var increment;
                    
                    percent -= 100;
                    percent *= -1;
                    
                    if ( !dot.vTop ) {
                        dot.vTop = dot.begintop;
                    }
                    
                    // first step
                    if ( percent < steps.first.left ) {
                        if ( !dot.frame.first ) {
                            getFrame( dot , 'first' );
                        }
                        
                        increment = dot.frame.first;
                    }
                    
                    // second step
                    else if ( percent < steps.second.left ) {
                        if ( !dot.frame.second ) {
                            getFrame( dot , 'second' );
                            dot.frame.second *= -1;
                        }
                        
                        increment = dot.frame.second;
                    }
                    
                    // third step
                    else if ( percent < steps.third.left ) {
                        if ( !dot.frame.third ) {
                            getFrame( dot , 'third' );
                        }
                        
                        increment = dot.frame.third;
                    }
                    
                    // direction de la vague
                    if ( !dot.dirmove ) {
                        dot.dirmove = between( 1 , 2 ) == 2 ? 1 : -1;
                        dot.limitmove = 20;
                        dot.manymove = 0;
                    }
                    
                    // incrémenter la vague
                    dot.manymove++;
                    
                    if ( dot.dirmove < 0 ) {
                        dot.radius = 1;
                        increment--;
                    }
                    else {
                        dot.radius = 2;
                        increment++;
                    }
                    
                    if ( dot.manymove > dot.limitmove ) {
                        dot.manymove = 0;
                        
                        dot.limitmove = 40;
                        dot.dirmove *= -1;
                    }
                    
                    // --------------------------------------------------------
                    // --------------------------------------------------------
                    
                    dot.vTop += increment;
                    
                    dot.top = dot.vTop - ( spectre / 2.5 );
                }
                
                return function ( data ) {
                    
                    // créer un point
                    if ( Date.now() - last > diff ) {
                        last = Date.now();
                        
                        dotlist.push( {
                            frame : {} ,
                            left : zero.left ,
                            radius : between( 1 , 2 ) ,
                            begintop : between( betweenZeroTop[ 0 ] , betweenZeroTop[ 1 ] )
                        } );
                    }
                    
                    // dessiner les points
                    for ( var i = 0 , l = dotlist.length , dot , percLeft , range ; i < l ; i++ ) {
                        dot = dotlist[ i ];
                        
                        if ( dot.left <= 0 ) {
                            dotlist.splice( i , 1 );
                            i--, l--;
                            continue;
                        }
                        
                        percLeft = ( dot.left / canvas.width ) * 100;
                        range = rangeRGB( percLeft );
                        
                        ctx.fillStyle = "rgb(" + range[ 0 ] + "," + range[ 1 ] + "," + range[ 2 ] + ")";
                        
                        ctx.beginPath();
                        ctx.arc( dot.left , dot.top , dot.radius , 0 , 2 * PI );
                        ctx.fill();
                        
                        dot.left -= calcFrame( duration , 0 , canvas.width );
                        
                        moveTop( dot , percLeft , data[ Math.round( percLeft ) * 4 ] );
                    }
                    
                };
                
            } )() ,
            
            'wave' : ( function () {
                
                var smoothyY = [];
                
                smoothyIncrement( {
                    begin : 0 ,
                    end : 230 ,
                    speed : 5000 ,
                    eachFrame : function ( total , increment ) {
                        smoothyY.push( increment );
                    }
                } );
                
                var point = [] ,
                    pointIndex = 0;
                
                var left = 0 ,
                    right = 800;
                
                while ( left < right ) {
                    
                    if ( !point[ pointIndex ] ) {
                        point[ pointIndex ] = [];
                    }
                    
                    point[ pointIndex ].push( {
                        x : left ,
                        y : between( 250 , 280 ) ,
                        yindexleft : 0 ,
                        yindexright : 0
                    } );
                    
                    point[ pointIndex ].push( {
                        x : left ,
                        y : between( 250 , 280 ) ,
                        yindexleft : 0 ,
                        yindexright : 0
                    } );
                    
                    point[ pointIndex ].push( {
                        x : left ,
                        y : between( 250 , 280 ) ,
                        yindexleft : 0 ,
                        yindexright : 0
                    } );
                    
                    left += 10;
                    pointIndex++;
                    
                }
                
                return function ( data ) {
                    
                    ctx.strokeStyle = "rgb(255,255,255)";
                    
                    point.forEach( function ( array ) {
                        
                        array.forEach( function ( point ) {
                            
                            ctx.beginPath();
                            ctx.moveTo( point.x , point.y );
                            ctx.lineTo( point.x + 8 , point.y );
                            ctx.stroke();
                            
                            point.x += 1;
                            
                            if ( smoothyY[ point.yindexleft ] ) {
                                point.y -= smoothyY[ point.yindexleft ];
                                point.yindexleft++;
                            }
                            else if ( smoothyY[ point.yindexright ] ) {
                                point.y += smoothyY[ point.yindexright ];
                                point.yindexright++;
                            }
                            else {
                                point.yindexleft = point.yindexright;
                                point.x = between( 250 , 280 );
                            }
                            
                        } );
                        
                    } );
                    
                };
                
            } )()
            
        } ,
        
        cls = {
            'bar' : 'glyphicon-equalizer' ,
            'mosaic' : 'glyphicon-th' ,
            'horizon' : 'glyphicon-align-center' ,
            'circle' : 'glyphicon-record' ,
            'building' : 'glyphicon-align-left' ,
            'eyes' : 'glyphicon-eye-open' ,
            'dust' : 'glyphicon-random'
        };
    
    ctx.lineWidth = 2;
    ctx.imageSmoothingEnabled =
        ctx.msImageSmoothingEnabled = false;
    
    {
        var btn = document.getElementById( 'bench' ) ,
            icon = btn.firstElementChild;
        
        events.listen( btn , 'click' , function () {
            var array = Object.keys( cls ) ,
                index = array.indexOf( type );
            
            if ( !array[ ++index ] ) {
                index = 0;
            }
            
            type = array[ index ];
            
            for ( var i in cls ) {
                icon.classList.remove( cls[ i ] );
            }
            
            icon.classList.add( cls[ type ] );
        } );
        
        icon.classList.add( cls[ type ] );
    }
    
    function render () {
        var r = getArray();
        frame( render );
        ctx.clearRect( 0 , 0 , canvas.width , canvas.height );
        draw[ type ]( r , max.apply( Math , r ) );
    }
    
    render();
    
} )();