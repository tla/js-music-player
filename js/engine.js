// ------------------------------------
// ------     audio controller
// ------------------------------------
function MusicConstructor ( id ) {
    this.element = document.getElementById( id );
    this.element.loop = this.element.autoplay = false;
    
    this.ogg = document.createElement( 'source' );
    this.ogg.setAttribute( 'type' , 'audio/ogg' );
    
    this.mp3 = document.createElement( 'source' );
    this.mp3.setAttribute( 'type' , 'audio/mp3' );
    
    this.element.appendChild( this.ogg );
    this.element.appendChild( this.mp3 );
}

MusicConstructor.prototype.play = function () {
    this.element.play();
};

MusicConstructor.prototype.paused = function () {
    return !!this.element.paused;
};

MusicConstructor.prototype.pause = function () {
    this.element.pause();
    events.dispatch( this.element , 'pause' );
};

MusicConstructor.prototype.switch = function () {
    if ( this.paused() ) {
        this.play();
    }
    else {
        this.pause();
    }
};

MusicConstructor.prototype.stop = function () {
    this.pause();
    this.element.currentTime = 0.0;
};

MusicConstructor.prototype.restart = function () {
    this.stop();
    this.play();
};

MusicConstructor.prototype.volume = function ( n ) {
    if ( n !== undefined ) {
        this.element.volume = +n / 100;
    }
    return this.element.volume;
};

MusicConstructor.prototype.mute = function ( b ) {
    if ( b !== undefined ) {
        this.element.muted = !!b;
    }
    return this.element.muted;
};

MusicConstructor.prototype.currentTime = function ( n ) {
    if ( n !== undefined ) {
        this.element.currentTime = +n;
    }
    return this.element.currentTime;
};

MusicConstructor.prototype.duration = function () {
    return this.element.duration;
};

MusicConstructor.prototype.source = function ( src ) {
    if ( src.slice( 0 , 5 ) == 'blob:' ) {
        if ( this.ogg.parentNode ) {
            this.element.removeChild( this.ogg );
            this.element.removeChild( this.mp3 );
        }
        
        this.element.setAttribute( 'src' , src );
    }
    else {
        this.ogg.setAttribute( 'src' , 'music/ogg/' + src + '.ogg' );
        this.mp3.setAttribute( 'src' , 'music/mp3/' + src + '.mp3' );
        
        this.element.removeAttribute( 'src' );
        
        if ( !this.ogg.parentNode ) {
            this.element.appendChild( this.ogg );
            this.element.appendChild( this.mp3 );
        }
    }
    
    this.element.load();
};

// ------------------------------------
// ------     init
// ------------------------------------
var _style = new cssgen() ,
    current_playing = 0 ,
    rangeCache = cache( 1000 ) ,
    events = new eventHandler() ,
    default_playlist = [
        {
            title : 'Mizuki\'s Last Chance - Yeah' ,
            src : 'Yeah'
        } ,
        {
            title : 'Zero Call - A40' ,
            src : 'Zero'
        } ,
        {
            title : 'Binaerpilot - Bend' ,
            src : '01'
        } ,
        {
            title : 'Diese - Loud Fly' ,
            src : '16'
        } ,
        {
            title : 'Kvaleoun - Free your mind' ,
            src : '05'
        } ,
        {
            title : 'Bo Marley & Volfoniq & Disrupt - L\'Anchoiade' ,
            src : 'bom'
        } ,
        {
            title : 'SmallRadio - LSF 7th Gear Remix' ,
            src : 'smallradio'
        } ,
        {
            title : 'Pornophonique - Space Invaders' ,
            src : '04'
        }
    ] ,
    track_list = [] ,
    colorPicker = document.getElementById( 'color-picker' ) ,
    Music = new MusicConstructor( 'music' ) ,
    seek_range = parseInt( document.getElementById( 'currentMove' ).getAttribute( 'max' ) ) ,
    update_display_seek = ( function () {
        var input = document.getElementById( 'currentMove' );
        
        return function () {
            input.value = seek_range / 100 * ( ( ( Music.currentTime() / 60 ) / 100 * 60 ) / ( ( Music.duration() / 60 ) / 100 * 60 ) * 100 );
        };
    } )() ,
    _r = document.getElementById( 'color-r' ) ,
    _g = document.getElementById( 'color-g' ) ,
    _b = document.getElementById( 'color-b' ) ,
    rgb = [ +_r.value , +_g.value , +_b.value ] ,
    gotoRGB = revertRGB( rgb[ 0 ] , rgb[ 1 ] , rgb[ 2 ] );

var _bubbles = [] , _selector = [];
typedtoarray( document.querySelectorAll( '.bubble-btn' ) ).forEach( function ( e ) {
    _bubbles.push( _awesomeBubble( e , { opacity : 60 } ) );
} );

function updateStylesheet () {
    var _rgb = 'rgb(' + rgb[ 0 ] + ',' + rgb[ 1 ] + ',' + rgb[ 2 ] + ')' ,
        _goto = 'rgb(' + gotoRGB[ 0 ] + ',' + gotoRGB[ 1 ] + ',' + gotoRGB[ 2 ] + ')';
    
    _bubbles.forEach( function ( bubble ) {
        bubble.setting.color = [ _rgb ];
        bubble.setting.colorEnd = [ _goto ];
    } );
    
    _selector.forEach( function ( _ ) {
        _style.remove( _ );
    } );
    _selector = [];
    
    function _add ( selector , rule ) {
        _selector.push( selector );
        _style.add( selector , rule );
    }
    
    _add(
        '.list-music .glyphicon-play,' +
        '.cedge.color .leftCedge,' +
        '#droped-zik .glyphicon,' +
        '#reset .glyphicon,' +
        'a.play-list:hover,' +
        'a' ,
        
        'color:' + _rgb
    );
    
    _add(
        '#color-picker' ,
        
        'background-color:' + _rgb + ';' +
        'border-color:' + _rgb + ';' +
        'color:' + _rgb
    );
    
    _add(
        'input[type=number].sub-button' ,
        
        'border-color:' + _rgb
    );
    
    _add(
        'input[type=number].sub-button:hover, ' +
        'input[type=number].sub-button:focus' ,
        
        'border-color:' + _goto
    );
    
    _add(
        '#repeat[data-status="normal"]' ,
        
        'color: white !important'
    );
    
    _add(
        '#repeat[data-status="all"]' ,
        
        'color:' + _rgb + ' !important'
    );
    
    _add(
        '#repeat[data-status="one"]' ,
        
        'color:' + _goto + ' !important'
    );
    
    _add(
        '#contain-list > p:hover > .glyphicon,' +
        '#droped-zik:hover .glyphicon,' +
        '#reset:hover .glyphicon,' +
        '.command .smart-btn:hover,' +
        'a:hover' ,
        
        'color:' + _goto
    );
    
    updateGradientBuffer();
}

function updateGradientBuffer () {
    var gradient = 'linear-gradient(to right' ,
        input = document.getElementById( 'currentMove' ) ,
        data = JSON.parse( input.getAttribute( 'data-buffer' ) ) ,
        _rgb = 'rgb(' + rgb[ 0 ] + ',' + rgb[ 1 ] + ',' + rgb[ 2 ] + ')';
    
    _style.remove( '#currentMove' );
    
    if ( !data ) return;
    
    data.forEach( function ( _ ) {
        var beginPercent = _[ 0 ] , percentLoad = _[ 1 ];
        
        gradient += ',transparent ' + beginPercent + '%, ' + _rgb + ' ' + beginPercent + '%, ' +
            ' ' + _rgb + ' ' + percentLoad + '%, transparent ' + percentLoad + '%';
    } );
    
    gradient += ')';
    
    _style.add(
        '#currentMove' ,
        
        'background-image: -webkit-' + gradient + ';' +
        'background-image: -moz-' + gradient + ';' +
        'background-image: -o-' + gradient + ';' +
        'background-image: ' + gradient + ';'
    );
}

updateStylesheet();

// ------------------------------------
// ------     tab index
// ------------------------------------
typedtoarray( document.querySelectorAll( '*' ) ).forEach( function ( e ) {
    e.setAttribute( 'tabindex' , '-1' );
} );

typedtoarray( document.querySelectorAll( 'input[type=number]' ) ).forEach( function ( e , i ) {
    e.setAttribute( 'tabindex' , ( i + 1 ).toString() );
} );

// ------------------------------------
// ------     music setting
// ------------------------------------
Music.volume( +document.getElementById( 'volume' ).value );

// ------------------------------------
// ------     number second to string xx:xx:xx
// ------------------------------------
var currentTime = ( function () {
    
    function msToTime ( ms ) {
        var date = new Date( parseInt( ms ) || 0 ) , zero = new Date( 0 );
        
        return {
            second : date.getSeconds() - zero.getSeconds() ,
            minute : date.getMinutes() - zero.getMinutes() ,
            hour : date.getHours() - zero.getHours()
        };
    }
    
    function around ( nb ) {
        return nb.toString().length < 2 ? '0' + nb : nb.toString();
    }
    
    return function ( sec ) {
        var data = msToTime( sec * 1000 );
        return around( data.hour ) + ':' + around( data.minute ) + ':' + around( data.second );
    };
    
} )();

// ------------------------------------
// ------     tooltip
// ------------------------------------
var URL = ( window.URL || window.webkitURL );

var indicator = ( function () {
    var indicator = document.getElementById( 'indicator' );
    
    return {
        
        show : function ( left , top , text ) {
            indicator.textContent = text;
            indicator.style.top = top + 'px';
            indicator.style.left = left + 'px';
            indicator.classList.add( 'plop' );
        } ,
        
        hide : function () {
            indicator.classList.remove( 'plop' );
        }
        
    };
} )();

function rgb2hexa ( r , g , b ) {
    return ( r.toString( 16 ) + g.toString( 16 ) + b.toString( 16 ) ).toUpperCase();
}

function hexa2rgb ( hexa ) {
    hexa = ( hexa.length == 3 ) &&
        ( hexa[ 0 ] + '0' + hexa[ 1 ] + '0' + hexa[ 2 ] + '0' ) ||
        ( hexa.length == 6 ) && hexa || false;
    
    return [
        Math.round( parseInt( hexa.slice( 0 , 2 ) , 16 ) ) ,
        Math.round( parseInt( hexa.slice( 2 , 4 ) , 16 ) ) ,
        Math.round( parseInt( hexa.slice( 4 ) , 16 ) )
    ];
}

function rgbToHsl ( r , g , b ) {
    r /= 255, g /= 255, b /= 255;
    var max = Math.max( r , g , b ) , min = Math.min( r , g , b );
    var h , s , l = ( max + min ) / 2;
    
    if ( max == min ) {
        h = s = 0; // achromatic
    }
    else {
        var d = max - min;
        s = l > 0.5 ? d / ( 2 - max - min ) : d / ( max + min );
        switch ( max ) {
            case r:
                h = ( g - b ) / d + ( g < b ? 6 : 0 );
                break;
            case g:
                h = ( b - r ) / d + 2;
                break;
            case b:
                h = ( r - g ) / d + 4;
                break;
        }
        h /= 6;
    }
    
    return [
        h * 360 , // h
        parseFloat( ( s * 100 ).toFixed( 2 ) ) , // s
        parseFloat( ( l * 100 ).toFixed( 2 ) ) // l
    ];
}

function hslToRgb ( h , s , l ) {
    var r , g , b , a = Math.abs , _r = Math.round;
    
    h /= 360 , s /= 100 , l /= 100;
    
    if ( s == 0 ) {
        r = g = b = l; // achromatic
    }
    else {
        function hue2rgb ( p , q , t ) {
            if ( t < 0 ) t += 1;
            if ( t > 1 ) t -= 1;
            if ( t < 1 / 6 ) return p + ( q - p ) * 6 * t;
            if ( t < 1 / 2 ) return q;
            if ( t < 2 / 3 ) return p + ( q - p ) * ( 2 / 3 - t ) * 6;
            return p;
        }
        
        var q = l < 0.5 ? l * ( 1 + s ) : l + s - l * s;
        var p = 2 * l - q;
        r = hue2rgb( p , q , h + 1 / 3 );
        g = hue2rgb( p , q , h );
        b = hue2rgb( p , q , h - 1 / 3 );
    }
    
    return [
        _r( a( r * 255 ) ) ,
        _r( a( g * 255 ) ) ,
        _r( a( b * 255 ) )
    ];
}

function revertRGB ( r , g , b ) {
    var hsl = rgbToHsl( r , g , b );
    
    hsl[ 0 ] += 180;
    if ( hsl[ 0 ] > 360 ) hsl[ 0 ] -= 360;
    
    //hsl[ 1 ] += 50;
    //if ( hsl[ 1 ] > 100 ) hsl[ 1 ] -= 100;
    
    //hsl[ 2 ] += 50;
    //if ( hsl[ 2 ] > 100 ) hsl[ 2 ] -= 100;
    
    hsl[ 1 ] = 100;
    hsl[ 2 ] = 50;
    
    return hslToRgb.apply( null , hsl );
}

function cache ( n ) {
    
    var c = [];
    !n && ( n = 200 );
    
    function SAVE ( k , v ) {
        SAVE[ k ] = ' ' + v , c.push( k );
        c.length > n && delete SAVE[ c.shift() ];
    }
    
    SAVE.prototype._purge = function () {
        while ( c.length ) delete SAVE[ c.shift() ];
        c = [];
    };
    
    return SAVE;
    
}

function konami ( fn ) {
    
    var konami = [ 38 , 38 , 40 , 40 , 37 , 39 , 37 , 39 , 66 , 65 , 13 ] , pressed = [] , timeout;
    
    events.listen( document , 'keydown' , function ( e ) {
        
        var i = 0 , check = 0;
        
        clearTimeout( timeout );
        timeout = setTimeout( function () { pressed = []; } , 2500 );
        
        pressed.push( e.which || e.keyCode );
        
        ( pressed.length <= konami.length ) || pressed.splice( 0 , 1 );
        
        for ( ; i < konami.length ; i++ ) {
            ( konami[ i ] !== pressed[ i ] ) || check++;
        }
        
        ( check !== konami.length ) || fn();
        
    } );
    
}

function resetBuffer () {
    document.getElementById( 'currentMove' ).removeAttribute( 'data-buffer' );
    updateGradientBuffer();
}

function prevent ( event ) {
    event.preventDefault();
    event.stopImmediatePropagation();
}

function htmlEscape ( str ) {
    return str
        .replace( /&/gi , '&amp;' )
        .replace( /"/gi , '&quot;' )
        .replace( /'/gi , '&apos;' )
        .replace( /</gi , '&lt;' )
        .replace( />/gi , '&gt;' );
}

function createObjectURL ( object ) {
    return URL.createObjectURL( object );
}

function revokeObjectURL ( object ) {
    return URL.revokeObjectURL( object );
}

function updateColorpickerValue () {
    colorPicker.value = '#' + rgb2hexa( rgb[ 0 ] , rgb[ 1 ] , rgb[ 2 ] );
}

// ------------------------------------
// ------     dubstep trololo ?
// ------------------------------------
konami( function () { switchMusic( false , 'trololo' ); } );

// ------------------------------------
// ------     switch music by playlist index or src
// ------------------------------------
function updatePlayNow () {
    document.getElementById( 'play-now' ).innerHTML = ( function () {
        var title = track_list[ current_playing ].title ,
            splited = title.split( '-' );
        
        if ( splited.length !== 2 ) {
            return htmlEscape( title );
        }
        
        return htmlEscape( splited[ 0 ] ) + ' - ' + '<span >' + htmlEscape( splited[ 0 ] ) + '</span>';
    } )();
}

function switchMusic ( number , src ) {
    var current = document.querySelector( '#music-' + current_playing + ' span' );
    
    Music.stop();
    
    current.classList.remove( 'glyphicon-play' );
    current.classList.add( 'glyphicon-chevron-right' );
    
    resetBuffer();
    
    if ( !number && !!src ) {
        Music.source( src );
    }
    else {
        current_playing = number;
        Music.source( track_list[ current_playing ].src );
        current = document.querySelector( '#music-' + current_playing + ' span' );
        
        current.classList.add( 'glyphicon-play' );
        current.classList.remove( 'glyphicon-chevron-right' );
    }
    
    updatePlayNow();
    
    setTimeout( function () {
        Music.play();
    } , 50 );
}

// ------------------------------------
// ------     build playlist
// ------------------------------------
function buildPlaylist ( playlist , keepPlay ) {
    var container = document.getElementById( 'contain-list' ) ,
        list = '' , t , f;
    
    container.innerHTML = '';
    track_list = playlist;
    current_playing = 0;
    
    for ( t in playlist ) {
        list += '<p id="music-' + t + '" ><span class="glyphicon glyphicon-chevron-right" ></span> &nbsp;&nbsp;' +
            '<a class="play-list" data-music="' + t + '" href="javascript:void(0)" >' + htmlEscape( track_list[ t ].title ) + '</a></p>';
    }
    
    container.innerHTML = list;
    f = document.querySelector( '#music-' + current_playing + ' span' );
    f.classList.remove( 'glyphicon-chevron-right' );
    f.classList.add( 'glyphicon-play' );
    
    if ( !keepPlay ) {
        Music.stop();
        Music.source( track_list[ current_playing ].src );
        document.getElementById( 'currentMove' ).value = 0;
    }
    
    updatePlayNow();
}

buildPlaylist( default_playlist , false );

// ------------------------------------
// ------     custom music
// ------------------------------------
( function () {
    var dropBtn = document.getElementById( 'droped-zik' ) ,
        spanFirst = dropBtn.querySelector( 'span:first-of-type' ) ,
        spanLast = dropBtn.querySelector( 'span:last-of-type' ) ,
        inputFile = document.getElementById( 'input-file' ) ,
        container = document.getElementById( 'container' ) ,
        custom = false;
    
    events.listen( document.getElementById( 'reset' ) , 'click' , function () {
        track_list.forEach( function ( track ) {
            track.blob && revokeObjectURL( track.src );
        } );
        
        buildPlaylist( default_playlist , false );
        
        resetBuffer();
        
        custom = false;
    } );
    
    function getPlayList ( files , r ) {
        for ( var i = 0 , l = files.length ; i < l ; i++ ) {
            r.push( {
                blob : true ,
                title : files[ i ].name ,
                src : createObjectURL( files[ i ] )
            } );
        }
        
        return r;
    }
    
    function dragLeave () {
        spanLast.classList.remove( 'glyphicon-ok' );
        spanFirst.classList.remove( 'glyphicon-ok' );
        spanLast.classList.add( 'glyphicon-hand-left' );
        spanFirst.classList.add( 'glyphicon-hand-right' );
    }
    
    // autorize dragover
    events.listen( dropBtn , 'dragover' , prevent );
    
    // anime on enter
    events.listen( dropBtn , 'dragenter' , function () {
        spanFirst.classList.remove( 'glyphicon-hand-right' );
        spanLast.classList.remove( 'glyphicon-hand-left' );
        spanFirst.classList.add( 'glyphicon-ok' );
        spanLast.classList.add( 'glyphicon-ok' );
    } );
    
    // anime on leave
    events.listen( dropBtn , 'dragleave' , dragLeave );
    
    // dropped
    events.listen( dropBtn , 'drop' , function ( e ) {
        var files = e.target.files || e.dataTransfer.files || null;
        
        prevent( e );
        
        dragLeave();
        
        !!files && files.length > 0 && ( buildPlaylist( getPlayList( files , custom ? track_list : [] ) , custom ) , custom = true );
    } );
    
    // input type file
    events.listen( dropBtn , 'click' , function () { inputFile.click(); } );
    
    function change () {
        this.files.length && ( buildPlaylist( getPlayList( this.files , custom ? track_list : [] ) , custom ), custom = true );
        inputFile = this.cloneNode( false );
        events.mute( this );
        container.removeChild( this );
        container.appendChild( inputFile );
        events.listen( inputFile , 'change' , change );
    }
    
    events.listen( inputFile , 'change' , change );
} )();

// ------------------------------------
// ------     color control
// ------------------------------------

function applyColor () {
    rangeCache.prototype._purge();
    rgb = [ +_r.value , +_g.value , +_b.value ];
    gotoRGB = revertRGB( rgb[ 0 ] , rgb[ 1 ] , rgb[ 2 ] );
    
    _bubbles.forEach( function ( bubble ) {
        bubble.setting.color = [ 'rgb(' + rgb[ 0 ] + ',' + rgb[ 1 ] + ',' + rgb[ 2 ] + ')' ];
        bubble.setting.colorEnd = [ 'rgb(' + gotoRGB[ 0 ] + ',' + gotoRGB[ 1 ] + ',' + gotoRGB[ 2 ] + ')' ];
    } );
    
    updateStylesheet();
}

events.listen( document.getElementById( 'toolbar' ) , 'keyup , input , change , blur' , 'input[type=number]' , function () {
    [ _r , _g , _b ].forEach( function ( input ) {
        if ( !( !!input.value ) || isNaN( +input.value ) || input.value < 0 ) {
            input.value = 0;
        }
        
        if ( input.value > 255 ) {
            input.value = 255;
        }
        
        input.value = parseInt( input.value );
    } );
    
    updateColorpickerValue();
    
    applyColor();
} );

events.listen( colorPicker , 'change, input' , function () {
    rgb = hexa2rgb( this.value.slice( 1 ) );
    
    _r.value = rgb[ 0 ];
    _g.value = rgb[ 1 ];
    _b.value = rgb[ 2 ];
    
    applyColor();
} );

updateColorpickerValue();

// ------------------------------------
// ------     play | pause
// ------------------------------------
events.listen( document.getElementById( 'play' ) , 'click' , function () {
    Music.switch();
} );

// ------------------------------------
// ------     mute
// ------------------------------------
events.listen( document.getElementById( 'mute' ) , 'click' , function () {
    var val = Music.volume() * 100 ,
        volume = document.getElementById( 'volume' ) ,
        mute = document.querySelector( '#mute span' );
    
    if ( Music.mute() ) {
        Music.mute( false );
        
        volume.value = val;
        volume.classList.remove( 'range-disable' );
        volume.removeAttribute( 'readonly' );
        
        mute.classList.remove( 'glyphicon-volume-up' );
        mute.classList.remove( 'glyphicon-volume-off' );
        mute.classList.remove( 'glyphicon-volume-down' );
        mute.classList.add( val <= 0 ? 'glyphicon-volume-off' : val < 50 ? 'glyphicon-volume-down' : 'glyphicon-volume-up' );
    }
    else {
        Music.mute( true );
        
        mute.classList.add( 'glyphicon-volume-off' );
        mute.classList.remove( 'glyphicon-volume-up' );
        mute.classList.remove( 'glyphicon-volume-down' );
        
        volume.classList.add( 'range-disable' );
        volume.setAttribute( 'readonly' , 'true' );
    }
} );

// ------------------------------------
// ------     change volume
// ------------------------------------
events.listen( document.getElementById( 'volume' ) , 'mousedown' , function () {
    var offsetLeft , offsetRight , top , mute = document.querySelector( '#mute span' );
    
    if ( Music.mute() ) return false;
    
    offsetLeft = this.offsetLeft + 1;
    offsetRight = offsetLeft + this.offsetWidth - 12;
    top = document.getElementById( 'currentMove' ).offsetTop + 75;
    
    events.listen( this , 'mousemove' , function ( e ) {
        var val = +this.value , left = e.clientX - 5;
        
        left < offsetLeft && ( left = offsetLeft );
        
        left > offsetRight && ( left = offsetRight );
        
        Music.volume( val );
        
        indicator.show( left , top , val );
        
        mute.classList.remove( 'glyphicon-volume-up' );
        mute.classList.remove( 'glyphicon-volume-off' );
        mute.classList.remove( 'glyphicon-volume-down' );
        mute.classList.add( val <= 0 ? 'glyphicon-volume-off' : val < 50 ? 'glyphicon-volume-down' : 'glyphicon-volume-up' );
    } );
    
    events.listen( this , 'mouseup' , function () {
        
        events.mute( this , 'mousemove , mouseup' );
        
        Music.volume( +this.value );
        
        indicator.hide();
        
    } );
} );

// ------------------------------------
// ------     back button
// ------------------------------------
events.listen( document.getElementById( 'back' ) , 'click' , function () {
    if ( current_playing - 1 >= 0 ) {
        switchMusic( +current_playing - 1 );
    }
    else {
        switchMusic( track_list.length - 1 );
    }
} );

// ------------------------------------
// ------     next button
// ------------------------------------
events.listen( document.getElementById( 'next' ) , 'click' , function () {
    if ( track_list.length - 1 > current_playing ) {
        switchMusic( +current_playing + 1 );
    }
    else {
        switchMusic( 0 );
    }
} );

// ------------------------------------
// ------     seek & bubble preview
// ------------------------------------
events.listen( document.getElementById( 'currentMove' ) , 'mousedown' , function () {
    var offsetLeft = this.offsetLeft + 1 ,
        offsetRight = offsetLeft + this.offsetWidth - 12 ,
        top = this.offsetTop + 25;
    
    events.mute( Music.element , 'timeupdate.input' );
    
    events.listen( this , 'mousemove' , function ( e ) {
        // preview
        var left = e.clientX - 5;
        
        left < offsetLeft && ( left = offsetLeft );
        
        left > offsetRight && ( left = offsetRight );
        
        // display preview + total
        indicator.show(
            left , top ,
            currentTime( ( Music.duration() / 100 * ( this.value / seek_range * 100 ) ) ) + ' | ' + currentTime( Music.duration() )
        );
    } );
    
    events.listen( this , 'mouseup' , function () {
        
        events.mute( this , 'mousemove , mouseup' );
        
        events.listen( Music.element , 'timeupdate.input' , update_display_seek );
        
        indicator.hide();
        
        // change time
        Music.currentTime( round( Music.duration() / 100 * ( +this.value / seek_range * 100 ) , 1 ) );
    } );
} );

// ------------------------------------
// ------     repeat mode
// ------------------------------------
( function () {
    
    var repeat = document.getElementById( 'repeat' );
    
    events.listen( repeat , 'click' , function () {
        var repeat = this.getAttribute( 'data-status' );
        
        //this.classList.remove( 'repeat-normal' );
        //this.classList.remove( 'repeat-all' );
        //this.classList.remove( 'repeat-one' );
        
        switch ( repeat ) {
            case 'normal':
                repeat = 'all';
                break;
            
            case 'all':
                repeat = 'one';
                break;
            
            case 'one':
                repeat = 'normal';
                break;
        }
        
        //this.classList.add( 'repeat-' + repeat );
        
        this.setAttribute( 'data-status' , repeat );
    } );
    
} )();

// ------------------------------------
// ------     audio event
// ------------------------------------
( function () {
    
    var playBtn = document.querySelector( '#play > span' ) ,
        loadingBar = document.getElementById( 'loading-bar' ) ,
        input = document.getElementById( 'currentMove' ) ,
        time = document.getElementById( 'current' );
    
    // on play
    events.listen( Music.element , 'playing' , function () {
        
        events.listen( this , 'timeupdate.input' , update_display_seek );
        
        events.listen( this , 'timeupdate' , function () {
            time.textContent = currentTime( Music.currentTime() ) + ' | ' + currentTime( Music.duration() );
        } );
        
        loadingBar.style.opacity = '0';
        
    } );
    
    // buffer bar
    setInterval( function () {
        var i = 0 ,
            duration ,
            gradient = [] ,
            buffer = Music.element.buffered;
        
        input.removeAttribute( 'data-buffer' );
        
        if ( buffer.length < 1 ) {
            return updateGradientBuffer();
        }
        
        duration = Math.floor( Music.duration() );
        
        function percent ( value ) {
            return Math.floor( 100 / duration * Math.floor( value ) );
        }
        
        for ( ; i < buffer.length ; i++ ) {
            gradient.push( [
                percent( buffer.start( i ) ) ,
                percent( buffer.end( i ) )
            ] );
        }
        
        input.setAttribute( 'data-buffer' , JSON.stringify( gradient ) );
        
        updateGradientBuffer();
    } , 1000 );
    
    // mask progress bar
    events.listen( Music.element , 'canplaythrough' , function () {
        loadingBar.style.opacity = '0';
    } );
    
    // show progress bar
    events.listen( Music.element , 'waiting' , function () {
        loadingBar.style.opacity = '1';
    } );
    
    // error
    events.listen( Music.element , 'error' , function () {
        alert( 'erreur inconnue' );
        Music.stop();
    } );
    
    // stop
    events.listen( Music.element , 'ended , pause' , function () {
        playBtn.classList.add( 'glyphicon-play' );
        playBtn.classList.remove( 'glyphicon-pause' );
    } );
    
    // play
    events.listen( Music.element , 'play , playing' , function () {
        playBtn.classList.add( 'glyphicon-pause' );
        playBtn.classList.remove( 'glyphicon-play' );
    } );
    
    events.listen( Music.element , 'stalled' , function () {
        alert( 'musique introuvable / incompatible' );
        this.load();
    } );
    
    // finish
    events.listen( Music.element , 'ended' , function () {
        var next = document.getElementById( 'next' );
        
        events.mute( Music.element , 'timeupdate.input' );
        
        setTimeout( function () {
            switch ( document.getElementById( 'repeat' ).getAttribute( 'data-status' ) ) {
                case 'normal':
                    track_list.length - 1 > current_playing && next.click();
                    break;
                
                case 'all':
                    next.click();
                    break;
                
                case 'one':
                    Music.restart();
                    break;
            }
        } , 500 );
    } );
    
} )();

// ------------------------------------
// ------     playlist event
// ------------------------------------
events.listen( document.getElementById( 'contain-list' ) , 'click' , '.play-list' , function () {
    switchMusic( +this.getAttribute( 'data-music' ) );
} , true );